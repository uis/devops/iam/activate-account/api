"""Activate Account URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.http import HttpResponse
from django.urls import include, path

# Django debug toolbar is only installed in developer builds
try:
    import debug_toolbar

    HAVE_DDT = True
except ImportError:
    HAVE_DDT = False

urlpatterns = [
    # use `healthy` rather than `healthz` as Cloud Run reserves the use of the `/healthz` endpoint
    path(
        "healthy",
        lambda request: HttpResponse("ok", content_type="text/plain"),
        name="healthy",
    ),
    path("", include("authentication.urls")),
    # You'll also need to update api/tests/test_versions_view.py
    # Include the base API urls - which gives a view of the available API versions
    path("", include("api.urls")),
    # Include the v1alpha urls - allowing for any release version, the version is validated
    # by setting REST_FRAMEWORK.ALLOWED_VERSIONS in settings.base
    path("v1alpha1/", include(("api.v1alpha.urls", "v1alpha1"), namespace="v1alpha1")),
]

if settings.INTERNAL_API_ENABLED:
    urlpatterns += [
        path(
            "data-manager-api/",
            include(("data_manager_api.urls", "data_manager"), namespace="data_manager_api"),
        ),
    ]

# Selectively enable django debug toolbar URLs. Only if the toolbar is
# installed *and* DEBUG is True.
if HAVE_DDT and settings.DEBUG:
    urlpatterns = [
        path(r"__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
