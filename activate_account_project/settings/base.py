import os
import sys
from datetime import timedelta

import externalsettings
import structlog
from corsheaders.defaults import default_headers

from api.versions import AVAILABLE_VERSIONS

from .._version import __version__

# By default, make use of connection pooling for the default database and use the Postgres engine.
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "CONN_MAX_AGE": 60,  # seconds
    },
}

INTERNAL_API_ENABLED = False
DATA_MANAGER_READ_ONLY = True
FAKE_RESET_TOKEN_IF_MISSING = False

# If the EXTRA_SETTINGS_URLS environment variable is set, it is a comma-separated list of URLs from
# which to fetch additional settings as YAML-formatted documents. The documents should be
# dictionaries and top-level keys are imported into this module's global values.
_external_setting_urls = []
_external_setting_urls_list = os.environ.get("EXTRA_SETTINGS_URLS", "").strip()
if _external_setting_urls_list != "":
    _external_setting_urls.extend(_external_setting_urls_list.split(","))

externalsettings.load_external_settings(
    globals(),
    urls=_external_setting_urls,
    required_settings=[
        "SECRET_KEY",
        "DATABASES",
        "PASSWORD_APP_RESET_TOKEN_URL",
        "PASSWORD_APP_TOKEN",
    ],
    optional_settings=[
        "EMAIL_HOST",
        "EMAIL_HOST_PASSWORD",
        "EMAIL_HOST_USER",
        "EMAIL_PORT",
        "INTERNAL_API_ENABLED",
        "DATA_MANAGER_READ_ONLY",
        "FAKE_RESET_TOKEN_IF_MISSING",
    ],
)

#: Base directory containing the project. Build paths inside the project via
#: ``os.path.join(BASE_DIR, ...)``.
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

#: SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

#: By default, all hosts are allowed.
ALLOWED_HOSTS = ["*"]

#: Installed applications
INSTALLED_APPS = [
    # Despite the fact we would maybe want to, we can't disable `django.contrib.auth` here,
    # because `knox` depends on `django.contrib.auth` (even if we not use the User model in the
    # Token model).
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "whitenoise.runserver_nostatic",  # use whitenoise even in development
    "django.contrib.staticfiles",
    "crispy_forms",
    "django_filters",
    "drf_spectacular",
    "rest_framework",
    "corsheaders",
    "knox",
    "activate_account",
    "api",
    "authentication",
]

#: Installed middleware
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django_structlog.middlewares.RequestMiddleware",
]
#: Root URL patterns
ROOT_URLCONF = "activate_account_project.urls"

#: Template loading
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

#: WSGI
WSGI_APPLICATION = "activate_account_project.wsgi.application"


#: Password validation
#:
#: .. seealso:: https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


#: Internationalization
#:
#: .. seealso:: https://docs.djangoproject.com/en/2.0/topics/i18n/
LANGUAGE_CODE = "en-gb"

#: Internationalization
TIME_ZONE = "UTC"

#: Internationalization
USE_I18N = True

#: Internationalization
USE_TZ = True

#: Static files (CSS, JavaScript, Images)
#:
#: .. seealso:: https://docs.djangoproject.com/en/2.0/howto/static-files/
STATIC_URL = "/static/"

#: Authentication backends
AUTHENTICATION_BACKENDS: list[str] = []

# Configure DRF to use Django's session authentication to determine the current user
REST_FRAMEWORK = {
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "authentication.authentication.TokenAuthentication",
    ],
    "DEFAULT_PERMISSION_CLASSES": [
        "authentication.permissions.IsAuthenticatedWithAccount",
    ],
    "DEFAULT_RENDERER_CLASSES": [
        "djangorestframework_camel_case.render.CamelCaseJSONRenderer",
    ],
    "DEFAULT_PARSER_CLASSES": [
        "djangorestframework_camel_case.parser.CamelCaseJSONParser",
        "djangorestframework_camel_case.parser.CamelCaseFormParser",
        "djangorestframework_camel_case.parser.CamelCaseMultiPartParser",
    ],
    "DEFAULT_VERSIONING_CLASS": "rest_framework.versioning.NamespaceVersioning",
    "ALLOWED_VERSIONS": AVAILABLE_VERSIONS,
    "DEFAULT_VERSION": "v1alpha1",
    "EXCEPTION_HANDLER": "authentication.errors.exception_handler",
}

KNOX_TOKEN_MODEL = "authentication.AuthToken"

REST_KNOX = {
    "TOKEN_TTL": timedelta(minutes=20),
    "AUTO_REFRESH_MAX_TTL": timedelta(minutes=30),
    "AUTO_REFRESH": True,
    "USER_SERIALIZER": "knox.serializers.UserSerializer",
    "AUTH_HEADER_PREFIX": "Bearer",
    "TOKEN_MODEL": "authentication.AuthToken",
}

# settings for the OpenAPI schema generator
# https://drf-spectacular.readthedocs.io/en/latest/settings.html?highlight=settings
SPECTACULAR_SETTINGS = {
    "TITLE": "Activate Account",
    "DESCRIPTION": "Activate Account API",
    "VERSION": __version__,
    "SERVE_INCLUDE_SCHEMA": False,
    "CONTACT": {
        "name": "UIS DevOps Division",
        "url": "https://guidebook.devops.uis.cam.ac.uk/en/latest/",
        "email": "devops@uis.cam.ac.uk",
    },
    "TAGS": [
        {
            "name": "authentication",
            "description": "Create and manage user authentication tokens, including login and "
            "logout functionalities.",
        },
        {
            "name": "account management",
            "description": "Endpoints related to retrieving and updating account details of the "
            "logged-in user.",
        },
        {
            "name": "api versioning",
            "description": "List and manage different versions of the API, providing access points"
            " to version-specific endpoints.",
        },
    ],
    "EXTENSIONS": [
        ("authentication.authentication.TokenAuthenticationExtension", "builtin"),
    ],
    "CAMELIZE_NAMES": True,
    "POSTPROCESSING_HOOKS": [
        "activate_account_project.spectacular.camelize_serializer_fields",
        "drf_spectacular.hooks.postprocess_schema_enums",
    ],
}

SPECTACULAR_COMPONENTS_KEEP_SNAKE_CASE = [
    "TokenRequest",
    "TokenResponse",
]

# Allow all origins to access API.
CORS_URLS_REGEX = r"^.*$"
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = (*default_headers, "X-Recaptcha-Token")

SWAGGER_SETTINGS = {
    # Describe token authentication in swagger definition
    "SECURITY_DEFINITIONS": {
        "Bearer": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header",
        },
    },
}


STATIC_ROOT = os.environ.get("DJANGO_STATIC_ROOT", os.path.join(BASE_DIR, "build", "static"))

# By default we a) redirect all HTTP traffic to HTTPS, b) set the HSTS header to a maximum age
# of 1 year (as per the consensus recommendation from a quick Google search) and c) advertise that
# we are willing to be "preloaded" into Chrome and Firefox's internal list of HTTPS-only sites.
# Set the DANGEROUS_DISABLE_HTTPS_REDIRECT variable to any non-blank value to disable this.
if os.environ.get("DANGEROUS_DISABLE_HTTPS_REDIRECT", "") == "":
    # Exempt the healtch-check endpoint from the HTTP->HTTPS redirect.
    SECURE_REDIRECT_EXEMPT = ["^healthy/?$"]

    SECURE_SSL_REDIRECT = True
    SECURE_HSTS_SECONDS = 31536000  # == 1 year
    SECURE_HSTS_PRELOAD = True
else:
    print("Warning: HTTP to HTTPS redirect has been disabled.", file=sys.stderr)
# We also support the X-Forwarded-Proto header to detect if we're behind a load balancer which does
# TLS termination for us.
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

# Whether to support the x_forwarded_host in constructing the canonical url.
# USE_X_FORWARDED_HOST = True

_structlog_foreign_pre_chain = [
    structlog.stdlib.add_log_level,
    structlog.stdlib.add_logger_name,
    structlog.processors.TimeStamper(fmt="iso"),
]


LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse",
        },
        "require_debug_true": {
            "()": "django.utils.log.RequireDebugTrue",
        },
    },
    "formatters": {
        # This formatter logs as structured JSON suitable for use in Cloud hosting environments.
        "json_formatter": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processor": structlog.processors.JSONRenderer(),
            "foreign_pre_chain": _structlog_foreign_pre_chain,
        },
        # This formatter logs as coloured text suitable for use by humans.
        "console_formatter": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processor": structlog.dev.ConsoleRenderer(colors=True),
            "foreign_pre_chain": _structlog_foreign_pre_chain,
        },
    },
    "handlers": {
        # If debug is enabled, we render using the pretty console formatter. If it is disabled we
        # render using the JSON formatter.
        "console": {
            "class": "logging.StreamHandler",
            "filters": ["require_debug_true"],
            "formatter": "console_formatter",
        },
        "json": {
            "class": "logging.StreamHandler",
            "filters": ["require_debug_false"],
            "formatter": "json_formatter",
        },
    },
    "loggers": {
        "": {
            "handlers": ["json", "console"],
            "propagate": True,
            "level": "INFO",
        },
    },
}

structlog.configure(
    processors=[
        structlog.contextvars.merge_contextvars,
        structlog.stdlib.filter_by_level,
        structlog.processors.TimeStamper(fmt="iso"),
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.UnicodeDecoder(),
        structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
    ],
    logger_factory=structlog.stdlib.LoggerFactory(),
    cache_logger_on_first_use=True,
)
