from copy import deepcopy

from django.conf import settings
from drf_spectacular.contrib.djangorestframework_camel_case import (
    camelize_serializer_fields as _camelize_serializer_fields,
)


def camelize_serializer_fields(result, generator, request, public):
    """
    By default, camelize_serializer_fields will camelize all serializer fields. This function does
    not know about any parser_classes and renderer_classes that are set on the view, so it will
    camelize all serializer fields. This function is a wrapper around the original function that
    will camelize all serializer fields, but will keep the schemas of the components in
    SPECTACULAR_COMPONENTS_KEEP_SNAKE_CASE in snake case. This is necessary because the OpenAPI
    schema definitions are used to generate the client SDKs, and the client SDKs expect the schema
    definitions to be in snake case.
    """
    snake_case_schemas = []

    # Find all schemas that should keep their snake case
    for (component_name, component_type), component in generator.registry._components.items():
        if (
            component_type == "schemas"
            and component_name in settings.SPECTACULAR_COMPONENTS_KEEP_SNAKE_CASE
        ):
            snake_case_schemas.append((component, deepcopy(component.schema)))

    # Camelize all serializer fields
    result = _camelize_serializer_fields(result, generator, request, public)

    # Restore the schemas that should keep their snake case
    for component, schema in snake_case_schemas:
        component.schema = schema

    return result
