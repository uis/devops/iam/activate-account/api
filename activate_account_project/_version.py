import functools
import tomllib
from pathlib import Path


@functools.cache
def get_version():
    """
    Retrieves the project's version number from pyproject.toml.
    This function is intended to ensure that the version is obtained from a
    single source of truth (pyproject.toml), avoiding duplication.

    Returns:
        str: The version number as defined in pyproject.toml (e.g. "0.1.0").

    Raises:
        FileNotFoundError: If pyproject.toml cannot be found at the expected location.
    """
    # Navigate to the pyproject.toml file located in the project root
    pyproject_path = Path(__file__).resolve().parents[1] / "pyproject.toml"

    # Open and parse the pyproject.toml file using tomllib
    with pyproject_path.open("rb") as f:
        pyproject_data = tomllib.load(f)

    # Return the version number from the [tool.poetry] section
    return pyproject_data["tool"]["poetry"]["version"]


# Define the __version__ attribute for the project
__version__ = get_version()
