from django.core.management import call_command
from django.test import TestCase


class SystemChecksTestCase(TestCase):
    def test_system_checks_pass(self):
        call_command("check")
