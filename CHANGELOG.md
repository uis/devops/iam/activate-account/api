# Changelog

## [1.2.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/1.2.0...1.2.1) (2025-02-27)

### Bug Fixes

* keep snake case in openapi schema token authentication ([417a7c8](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/417a7c84a69578cc8bf714c1111b00df8314bb9c))

## [1.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/1.1.0...1.2.0) (2025-02-25)

### Features

* audit log successful and failed logins and lockout deletions ([dfb715d](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/dfb715d5c39d6006c3bdc21cc906a646e0b92df7))

## [1.1.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/1.0.0...1.1.0) (2025-02-19)

### Features

* add allowed recaptcha header ([c5e4774](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/c5e477451ba998c9c4c8f37a80e92a317b9be54d))

### Bug Fixes

* include cors app correctly ([3ca45ce](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/3ca45ce1cb8800928b434d29fef8f173dc22789d))

## [1.0.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.19.0...1.0.0) (2025-02-19)

### ⚠ BREAKING CHANGES

* add internal API endpoint for deleting lockouts

### Features

* add internal API endpoint for deleting lockouts ([5f7f190](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/5f7f1902cbaaae64c6070b1fa09635f9a6710dd1))

## [0.19.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.18.0...0.19.0) (2025-02-13)

### Features

* normalise last_name from name and in auth check ([afdf23e](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/afdf23ea7e732ca1b50e19fc211c77ba660d11d9))
* remove db case-insensitivity on last name and uppercase in normalisation ([67f5fd8](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/67f5fd8db116f08e44b4937f7c4aebd6138c5341))
* remove last_name from account_details view and requirement in data_manager_api view ([af5059b](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/af5059bbd6ebc045b290cd13b774379a089fbc6c))

### Bug Fixes

* catch MultipleObjectsReturned now possible with LIKE query ([355f315](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/355f315e277ff5bfa46c3da3f8b0137fcaf3c613))

## [0.18.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.17.1...0.18.0) (2025-02-11)

### Features

* add account lockout mechanism ([bb7bfa0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/bb7bfa08a61613cd77f9f7f32773b7c829be4c81))

## [0.17.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.17.0...0.17.1) (2025-02-07)

### Bug Fixes

* correct read only create behaviour ([d37847b](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/d37847b67425421b781e1c870659eb907a96f447))

## [0.17.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.16.1...0.17.0) (2025-02-04)

### Features

* replace code with list of codes ([80ecea5](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/80ecea5104bc6db23c8f856644853899d833eb01))

## [0.16.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.16.0...0.16.1) (2025-01-31)

### Bug Fixes

* bug with fake token creation ([4682f27](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/4682f27b98070b746ca6cf0e3800117f19757f63))

## [0.16.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.15.1...0.16.0) (2025-01-31)

### Features

* allow creation fake tokens in test/demo environments ([f4c77c1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/f4c77c1ae5fda2a0ecb6c4d2efa4adde4dea39dd))

## [0.15.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.15.0...0.15.1) (2025-01-22)

### Bug Fixes

* add openapi and clients to release assets ([58f8c22](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/58f8c22bb98551d3a070d38296ea8953a902e5b2))

## [0.15.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.14.0...0.15.0) (2025-01-22)

### Features

* handle order of events, add soft delete and add valid_at ([fb4243a](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/fb4243ad8982b0a910509f50bc4665b76deac7aa))

## [0.14.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.13.0...0.14.0) (2025-01-17)

### Features

* correct password app reset token call and handle missing users ([f6bee38](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/f6bee38e501f1b3afae48fde786934d0d987bcb9))

### Bug Fixes

* correct and extend reset-token tests ([1206cd1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/1206cd19283e4a480b8b894a521a0fe9add4bf32))

## [0.13.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.12.0...0.13.0) (2025-01-16)

### Features

* mock password app in docker compose ([76fe3e9](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/76fe3e974636cf991ed91f1b250ace7cc30bb785))
* reset token retrieval endpoint ([6669241](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/6669241df542b8ca04d0e94a7f976f819c2fc7b4))
* restructure and add token tests ([66e83d8](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/66e83d89c3ee2efd3879df4c092f76ab8e77390f))

### Bug Fixes

* bump ci template versions ([ee68366](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/ee683666e50602ef370030cdd3f035aa6733719d))
* ignore missing mypy requests stubs ([cc7020c](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/cc7020cf1076993a9080e3fb7244005f7ce21cef))

## [0.12.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.11.1...0.12.0) (2025-01-09)

### Features

* allow data-manager API to be read only ([e3ac61d](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/e3ac61d27e167468e6aa59c2c81c0b94a62e0dec))

## [0.11.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.11.0...0.11.1) (2025-01-08)

### Bug Fixes

* docker build by explicitly installing poetry-plugin-export ([a9e24a9](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/a9e24a93886bb6b338179b5e7b7c2b203cedbe1a))
* ensure OpenAPI packages are published for tagged releases ([aa230b1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/aa230b1a7fd8c003abf4de2197eddd60c8d46d56))

## [0.11.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.10.0...0.11.0) (2024-12-13)

### Features

* add endpoint for data manager to create/update/delete accounts ([8a90ac7](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/8a90ac7153d8e89573429456872a326690f82b01))

## [0.10.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.9.4...0.10.0) (2024-12-09)

### Features

* add generated documentation for OpanAPI clients ([ccd693d](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/ccd693d9ceb495193513f0cb42781e889cc59268))
* add some basic documentation ([17e6c7d](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/17e6c7dd47d0e17833ec7c3c42010d0fb6be42f6))
* document changes to OpenAPI generator template ([18e0123](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/18e01238d62a82d2befadaa3888d1f079e47438a))
* generate and package OpenAPI clients ([09825b1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/09825b18a0ba988dca97e7ad9206a8cc7a334dd5))
* move to upstream OpenAPI generator template ([b1fb6ea](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/b1fb6eae582f90b0e8891ea6477b2af76adadbca)), closes [uis/devops/continuous-delivery/ci-templates#93](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/issues/93) [#22](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/issues/22)
* only trigger API client generation if schema changes ([fa97e7f](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/fa97e7f7d0dd594bce6dc316e45c3eca4c8a363e))
* publish OpenAPI clients to GitLab's package registry ([a3ddcf2](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/a3ddcf2f6d08bc3d8738c3ba79397de85e5ce1e9))

### Bug Fixes

* add missing __init__.py files for management command module ([ff17e5c](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/ff17e5cb88825fa41455efe24635bc789e901d3a))
* correct various documentation warts ([470c48b](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/470c48b5c9b8da0c01c6f2a63046ae9333cb87a9))
* **pre-commit:** update flake8 ([f734811](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/f7348119b98ec8d975b7182111852e0433e0e629))
* **README:** add link to the GitLab pages now that it is known ([ddc634a](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/ddc634a6cd37e0f4176dd431ab683df168ae0af7))

## [0.9.4](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.9.3...0.9.4) (2024-12-03)

### Bug Fixes

* disable release assets for the moment ([9bb98c5](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/9bb98c56be68e7f9c729fd8ec803a9e5d3787060))

## [0.9.2](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.9.1...0.9.2) (2024-12-02)

### Bug Fixes

* tall release-it we are using a later GitLab ([4cbcdc0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/4cbcdc008f33ecd7f3b39228f75e585e328d9b2e))

## [0.9.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.9.0...0.9.1) (2024-12-02)

### Bug Fixes

* configure release-it with correct openapi asset name ([661af46](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/661af462e8ec6391db9bcab12795e4b4c093b767)), closes [/gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/-/jobs/1913998#L56](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account//gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/-/jobs/1913998/issues/L56)

## [0.9.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.8.0...0.9.0) (2024-12-02)

### Features

* add OpenAPI schema to release artefacts ([06a51ef](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/06a51efe6cb6c8a538f714f5d67fd6a84af56829))

### Bug Fixes

* ensure OpenAPI artefact paths exist in before_scripts ([546bfab](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/546bfabcab2f9604544498dd7416d19e9f63b09b))

## [0.8.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.7.0...0.8.0) (2024-11-28)

### Features

* add the bones of an OpenAPI client generation mechanism ([929894f](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/929894f5209d90ce0439e80f321f980091f449a8)), closes [#24](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/issues/24)

## [0.7.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.6.0...0.7.0) (2024-11-27)

### Features

* add `generate-schema` task to Poe configuration ([5e9325e](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/5e9325ee35ced508c5832c10dc4319425defe457))
* add custom MethodNotAllowedErrorSerializer ([412e468](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/412e4681b82ea5d078a63fffd0b83fa5b737eabf))
* add generated openapi.yaml ([b092c51](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/b092c513614af897a84e763cd3349791b6618ea0))
* add openapi-spec-validator ([11cc102](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/11cc102e1a6642f85b6ba1d97eaeb619c93fa233))
* add operation IDs to API endpoints for enhanced documentation clarity ([f9d6e61](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/f9d6e6112287a18455e33a3a0f355c498de76b24))
* add TokenResponseSerializer & extend_schema for LoginView ([64495fe](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/64495fe366c83da39a06388735a3445cf25f1e07))
* **api:** add `TokenAuthenticationExtension` for OpenAPI schema support ([28c8f40](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/28c8f404c4f74624ac8942e6683b7e9d5f62bc55))
* **api:** enhance AccountDetailsView with detailed OpenAPI response schemas ([fa5f170](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/fa5f1706ff2d948160c11ba577ab876963437e09))
* **auth:** add `EmptySerializer` to token management views ([1306986](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/13069865db8747ba5dab3d51a8415664d2a6a077))
* **authentication:** add TokenErrorSerializer for OAuth2 error handling ([8d36fb2](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/8d36fb2f11efe9ed69e706d745bfc92f3e95640e))
* **authentication:** enhance LogoutView & LogoutAllView with detailed OpenAPI response schemas ([df6038f](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/df6038f706065e1b2b402917c79c28b17488a170))
* **docs:** add OpenAPI specification generation instructions ([4307935](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/43079354c9c01805d88637a1d858fcd44487740e))
* **settings:** add version retrieval from `pyproject.toml` ([0b88af1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/0b88af1ca72fe5ee4f5cf15aec9aaf47bbec1e86))
* update based on review ([6e41ad7](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/6e41ad76bf6f3325a5ba26636b62d32c691ccd74))

### Bug Fixes

* **api:** update version of api in path ([7dc591f](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/7dc591fc6de0026f43453c3ea612d8cbf7eb4ae4))
* **openapi:** set DEFAULT_VERSION to v1alpha1 to fix missing account endpoints in spec ([c546ab7](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/c546ab7a7e38145e8351d82ac22bcbe4472942ee))
* update description of 405 error ([72a9f43](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/72a9f4397e25e7012a6e9810f2911686141b5959))
* update formatting ([e08d42c](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/e08d42ce5e38b2139be768f8c120e2e38110bb3b))
* update isort formatting ([614a83c](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/614a83c25dd0ab2e6829c34f9d864fc0e7b963cd))

## [0.6.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.5.0...0.6.0) (2024-11-13)

### Features

* **config:** update token TTL and auto-refresh max TTL in REST_KNOX ([9828489](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/9828489a514f8b8b5a52ba3c169bcf80acb35033))

## [0.5.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.4.1...0.5.0) (2024-11-11)

### Features

* add management command to delete accounts ([c3c9bb9](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/c3c9bb9952f6f4d4d3c7c894355b323f50726320))

## [0.4.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.4.0...0.4.1) (2024-11-11)

### Bug Fixes

* enhance AccountFactory attributes and update README.md ([a521545](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/a5215458819020d860a84be7cbfaa9b2707f091b))

## [0.4.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.3.1...0.4.0) (2024-11-01)

### Features

* ensure crsid field is always lower cased ([8ce476a](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/8ce476a0acda3663b15b1892fc337d4c57687775))

### Bug Fixes

* add posargs to tox config ([9618ce1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/9618ce15e57f754edc0a2c3362814a0ee9e42be8))

## [0.3.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.3.0...0.3.1) (2024-10-30)

### Bug Fixes

* allow revoking tokens ([ef459ee](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/ef459ee2404750525e7ac50f778e5f6ee4e3a848))
* print mock user details on creation ([b3305a9](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/b3305a970503988f32b0a842c5ac16d38c5cd152))

## [0.3.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.2.1...0.3.0) (2024-10-28)

### Features

* add account details model ([bf1cfdf](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/bf1cfdf52aa8fe263da21196f12f06cf2e7379fe))
* add session creation and return mechanism to API

## [0.2.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.2.0...0.2.1) (2024-10-28)

### Bug Fixes

* **tox.ini:** set build dir to match where CI jobs expect it to be ([7aee8ff](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/7aee8ff7ad1d23269df890593f2d3020cfcaec14))

## [0.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/compare/0.1.0...0.2.0) (2024-10-17)

### Features

* update renovate-bot config ([5042acf](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/5042acf5935eda1c58878f4ff9f99724c5330815))

### Bug Fixes

* correct renovate config ([c41f624](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/api/commit/c41f624b6a09ef93b74cfb64cef5635e45f7c2a1))
