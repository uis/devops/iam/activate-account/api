# Unique project name. Used by docker compose to avoid re-using docker containers with the same
# name from different projects.
# This is a dummy line to demonstrate that the upgrade works
name: activate_account

# Common environment variables needed for starting the Django application.
x-django-application-environment: &django-application-environment
  EXTERNAL_SETTING_SECRET_KEY: fake-secret
  EXTERNAL_SETTING_DATABASES: '{"default":{"HOST":"db","NAME":"webapp-db","USER":"webapp-user","PASSWORD":"webapp-pass"}}'
  # In production, we have one Cloud Run instance which is public facing and has these endpoints
  # disabled and one instance that is internal and has these endpoints enabled. In local
  # development we simplify the setup and just run one instance with all endpoints, internal and
  # external.
  EXTERNAL_SETTING_INTERNAL_API_ENABLED: "1"
  EXTERNAL_SETTING_DATA_MANAGER_READ_ONLY: "0"
  # Connect to mock password app reset token request endpoint
  EXTERNAL_SETTING_PASSWORD_APP_RESET_TOKEN_URL: "http://mock-password-app:8010/acc-reset-token"
  EXTERNAL_SETTING_PASSWORD_APP_TOKEN: "fake-token"
  # Disable HTTP -> HTTPS redirect when running locally.
  DANGEROUS_DISABLE_HTTPS_REDIRECT: "1"

# Dependencies for the webapp itself.
x-webapp-depends-on: &webapp-depends-on
  db:
    condition: service_healthy
  migrate-db:
    condition: service_completed_successfully

services:
  # App running in development mode. This can be run via:
  #   docker compose --profile development up --build
  webapp: &webapp
    build:
      context: .
      target: development
    environment:
      <<: *django-application-environment
    env_file:
      - secrets.env
    tty: true
    profiles:
      - development
    ports:
      - 8000:8000
      - 5678:5678
    depends_on:
      <<: *webapp-depends-on
    volumes:
      - .:/usr/src/app:ro

  # App running in production mode. This can be run via:
  #   docker compose --profile production up --build
  webapp-production:
    <<: *webapp
    build:
      context: .
    depends_on:
      <<: *webapp-depends-on
    profiles:
      - production
    environment:
      <<: *django-application-environment
    volumes: []

  # Service to allow management commands to be run via:
  #   docker compose run --build --rm manage ...
  manage:
    build:
      context: .
      target: development
    environment:
      <<: *django-application-environment
    env_file:
      - secrets.env
    tty: true
    entrypoint: ["./manage.py"]
    profiles:
      - utilities
    depends_on:
      db:
        condition: service_healthy
    volumes:
      # Management command may modify the application code.
      - .:/usr/src/app

  # Service to allow running tox within the application container via
  #   docker compose run --build --rm tox
  tox:
    build:
      context: .
      target: tox
    environment:
      <<: *django-application-environment
      TEST_USE_EXTERNAL_DATABASE: "1"
      EXTERNAL_SETTING_PASSWORD_APP_RESET_TOKEN_URL: "http://mocked/acc-reset-token"
      # Make sure that we point tox our writeable volume
      COVERAGE_FILE: /tox/coverage
      TOXINI_WORK_DIR: /tox/work
      TOXINI_ARTEFACT_DIR: /tox/build
    tty: true
    profiles:
      - utilities
    depends_on:
      db:
        condition: service_healthy
    volumes:
      - .:/usr/src/app:ro
      - ./build/compose/tox:/tox

  # A service which runs database migrations and then exits.
  migrate-db:
    build:
      context: .
      target: development
    environment:
      <<: *django-application-environment
    env_file:
      - secrets.env
    profiles:
      - development
      - production
    entrypoint: ["./manage.py"]
    command: ["migrate"]
    depends_on:
      db:
        condition: service_healthy
    volumes:
      - .:/usr/src/app:ro

  # A PostgreSQL database.
  db:
    image: postgres
    environment:
      POSTGRES_DB: webapp-db
      POSTGRES_USER: webapp-user
      POSTGRES_PASSWORD: webapp-pass
    healthcheck:
      # Note that "pg_isready" is not quite what we want since pg_isready only checks for
      # connection, not that the database exists.
      test: >-
        PGPASSWORD=$$POSTGRES_PASSWORD psql
        --username=$$POSTGRES_USER --dbname=$$POSTGRES_DB
        --host=127.0.0.1 --quiet --no-align --tuples-only
        -1 --command="SELECT 1"
      interval: 1s
      start_period: 60s
    ports:
      - 9876:5432
    profiles:
      - development
      - production
      - utilities
    volumes:
      - postgres-data:/var/lib/postgresql/data
      - postgres-backup:/backups

  mock-password-app:
    image: python:3.11-slim
    ports:
      - 8010:8010
    profiles:
      - development
      - production
    command: ["python", "-m", "http.server", "-d", "/usr/src/app", "8010"]
    volumes:
      - ./fake-token.json:/usr/src/app/acc-reset-token:ro

volumes:
  # Persistent volumes for postgres database data.
  postgres-data:
  postgres-backup:
