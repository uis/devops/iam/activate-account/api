# Activate Account

This repository hosts the API for the Activate Account service, which manages the signup and account activation
processes as part of the transition away from the legacy Raven Login system. The API enables integration with a new web
application designed to streamline user registration and initial account setup.

More information is available in the [developer documentation hosted on GitLab
Pages](https://api-uis-devops-iam-activate-account-1e0d417baa8a52873ee0a121002.uniofcam.dev/).

## Documentation

The project has detailed documentation for developers, including a "getting
started" guide.

To generate documentation locally, install dependencies via `poetry install` and run a local
documentation server via `poetry run mkdocs serve`.

## Guidebook - Quickstart and Testing

To know how to get started with this project visit our [Guidebook](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/webapp-dev-environment/).

To view notes on how to get the project working locally visit the [getting started section](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/webapp-dev-environment/#getting-started-with-a-project) of the Guidebook.

To get started with Testing visit our [testing section](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/webapp-dev-environment/#running-tests) of the Guidebook.

## Loading secrets at runtime

If the `EXTRA_SETTINGS_URLS` environment variable is non-empty it is interpreted
as a comma-separated set of URLs from which to fetch settings. Settings are
fetched and applied in the order they are listed.

The settings should be in the form of a YAML document which is fetched, parsed
and interpolated into the Django settings when the server starts.

`EXTRA_SETTINGS_URLS` currently understands the following URL schemes:

* file://... URLs are loaded from the local file system. If the URL
  lacks any scheme, it is assumed to be a file URL.
* https://... URLs are fetched using HTTP over TLS.
* gs://BUCKET/LOCATION formatted URLs specify a Google Cloud Storage
  bucket and a location within that bucket of an object to load settings
  from.
* sm://PROJECT/SECRET#VERSION formatted URLs specify a Google Secret
  Manager secret to load settings from. If the version is omitted, the
  latest version is used.

For Google Cloud Storage and Secret Manager URLs, application default
credentials are used to authenticate to Google.

Settings which can be loaded from external YAML documents can also be specified
in environment variables. A variable of the form EXTERNAL_SETTING_[NAME] is
imported as the setting "NAME" and the value of the variable is interpreted as a
YAML formatted value for the setting.

## Notes on debugging

The Full-screen console debugger `pudb` has been included to allow you to run a debug in the
docker-compose environment. To use, simply set the breakpoint using `import pdb; pdb.set_trace()`
and attach to the container using:

```bash
docker attach activate_account_development_app_1
```

For a fuller description of how to debug follow the
[guide to debugging with pdb and Docker](https://blog.lucasferreira.org/howto/2017/06/03/running-pdb-with-docker-and-gunicorn.html)
(it works just as well for `pudb`).

## CI configuration

The project is configured with Gitlab AutoDevOps via Gitlab CI using the .gitlab-ci.yml file.


## Generating the OpenAPI Specification

To generate the OpenAPI specification for this project, you can use the configured Poetry task. Run the following command:

```bash
poetry poe generate-schema
```

This command utilises the `poe` task `generate-schema` to build and run the necessary Docker commands, generating the `openapi.yaml` file which contains the OpenAPI schema for the API.

## Copyright License

See the [LICENSE.txt](LICENSE.txt) file for details.
