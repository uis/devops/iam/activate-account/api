from dataclasses import dataclass

import requests
import structlog
from django.conf import settings
from tenacity import (
    retry,
    retry_if_not_exception_type,
    stop_after_attempt,
    wait_exponential,
)

logger = structlog.get_logger(__name__)

PASSWORD_APP_CONNECTION_TIMEOUT = 2
PASSWORD_APP_READ_TIMEOUT = 10
PASSWORD_APP_RETRIES = 3
PASSWORD_APP_INITIAL_RETRY_DELAY = 1
PASSWORD_APP_TOKEN_DURATION = 24 * 60 * 60  # 24 hours


@dataclass
class PasswordAppTokenResponse:
    token: str


class PasswordAppNotFound(Exception):
    message = "Password App was unable to find the user"


@retry(
    reraise=True,
    stop=stop_after_attempt(PASSWORD_APP_RETRIES + 1),
    wait=wait_exponential(multiplier=2, min=PASSWORD_APP_INITIAL_RETRY_DELAY),
    # Don't retry if the user wasn't found by the Password App
    retry=retry_if_not_exception_type(PasswordAppNotFound),
)
def get_reset_token(crsid: str, duration=PASSWORD_APP_TOKEN_DURATION) -> PasswordAppTokenResponse:
    """
    Retrieve a new reset token for the given CRSid from the Password App.

    Low confidence in the Password App, so we perform some basic retry logic before
    reporting the failure back to frontend.
    """

    try:
        request = requests.get(
            settings.PASSWORD_APP_RESET_TOKEN_URL,
            headers={"Authorization": f"Token {settings.PASSWORD_APP_TOKEN}"},
            params={"id": crsid, "duration": duration},
            timeout=(PASSWORD_APP_CONNECTION_TIMEOUT, PASSWORD_APP_READ_TIMEOUT),
        )
        request.raise_for_status()
        return PasswordAppTokenResponse(**request.json())
    except requests.HTTPError as e:
        if e.response.status_code == 404:
            logger.error(
                "get_reset_token_not_found",
                error="Password reset token retrieval failed: User not found",
                exc=repr(e),
            )
            raise PasswordAppNotFound
        logger.error(
            "get_reset_token_http_error",
            error="Password reset token retrieval failed: HTTP error",
            exc=repr(e),
        )
        raise e
    except requests.RequestException as e:
        logger.error(
            "get_reset_token",
            error="Password reset token retrieval failed: Network issue",
            exc=repr(e),
        )
        raise e
