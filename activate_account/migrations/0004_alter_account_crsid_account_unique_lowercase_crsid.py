# Generated by Django 4.2.14 on 2024-11-01 13:00

import django.db.models.functions.text
from django.db import migrations, models

import activate_account.models


class Migration(migrations.Migration):
    dependencies = [
        ("activate_account", "0003_accountdetails_terms_accepted"),
    ]

    operations = [
        migrations.AlterField(
            model_name="account",
            name="crsid",
            field=activate_account.models.CRSIdField(
                max_length=20, primary_key=True, serialize=False
            ),
        ),
        migrations.AddConstraint(
            model_name="account",
            constraint=models.UniqueConstraint(
                django.db.models.functions.text.Lower("crsid"), name="unique_lowercase_crsid"
            ),
        ),
    ]
