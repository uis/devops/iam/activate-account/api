# Generated by Django 4.2.14 on 2024-10-28 16:29

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("activate_account", "0002_accountdetails"),
    ]

    operations = [
        migrations.AddField(
            model_name="accountdetails",
            name="terms_accepted",
            field=models.BooleanField(default=False),
        ),
    ]
