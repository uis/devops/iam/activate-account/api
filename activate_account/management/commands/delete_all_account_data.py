from pprint import pprint

from django.core.management.base import BaseCommand

from activate_account.models import Account


class Command(BaseCommand):
    help = "Deletes account data in databases"

    def handle(self, *args, **options):
        account_count = Account.objects.all().count()
        if account_count == 0:
            self.stdout.write(self.style.SUCCESS("No accounts to delete."))
            return

        self.stdout.write(self.style.WARNING("Deleting account data in database..."))
        self.stdout.write(f"There are currently {account_count} accounts in the database.")
        self.stdout.write(self.style.WARNING("Are you sure you want to continue?"))

        result = input("Enter 'yes' to delete all data: ")
        if result.lower() == "yes":
            pprint(Account.objects.all().delete())
        else:
            self.stdout.write("Not deleting any objects.")
