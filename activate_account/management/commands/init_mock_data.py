from datetime import date
from pprint import pprint

from django.core.management.base import BaseCommand

from activate_account.factories import AccountFactory
from activate_account.models import Account
from data_manager_api.serializers import AccountDataSerializer
from data_manager_api.tests.utils import data_account_factory


class Command(BaseCommand):
    help = "Initializes mock data for testing purposes"

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS("Starting to initialize mock data..."))

        for _ in range(10):
            account = AccountFactory.build()
            data = data_account_factory(account)
            valid_at = account.valid_at

            serializer = AccountDataSerializer(data=data, valid_at=valid_at)
            serializer.is_valid(raise_exception=True)

            instance = serializer.save()

            account = Account.objects.get(crsid=instance.crsid)
            print("Created user:")
            pprint(
                {
                    "crsid": account.crsid,
                    "name": account.account_details.name,
                    "last_name": account.account_identifier.first().last_name,
                    "date_of_birth": date.strftime(
                        account.account_identifier.first().date_of_birth, "%Y-%m-%d"
                    ),
                    "code": account.account_identifier.first().code,
                },
                sort_dicts=False,
            )

        self.stdout.write(self.style.SUCCESS("Successfully initialized mock data."))
