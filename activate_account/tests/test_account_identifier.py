from datetime import date

import pytest
from django.db import IntegrityError

from activate_account.factories import AccountFactory, AccountIdentifierFactory
from activate_account.models import AccountIdentifier

pytestmark = pytest.mark.django_db


def test_account_uniqueness_together(account_identifier):
    with pytest.raises(IntegrityError):
        AccountIdentifierFactory(
            last_name=account_identifier.last_name,
            date_of_birth=account_identifier.date_of_birth,
            code=account_identifier.code,
        )


def test_same_code_across_different_accounts():
    identifier_code = "SHARED_CODE"

    account1 = AccountFactory()
    account2 = AccountFactory()

    identifier1 = AccountIdentifierFactory.create(
        code=identifier_code, last_name="Doe", date_of_birth=date(2000, 1, 1), account_id=account1
    )
    assert AccountIdentifier.objects.filter(account_id=account1).count() == 1

    identifier2 = AccountIdentifierFactory.create(
        code=identifier_code,
        last_name="Smith",
        date_of_birth=date(2000, 1, 1),
        account_id=account2,
    )

    assert AccountIdentifier.objects.filter(account_id=account2).count() == 1

    assert identifier1.account_id != identifier2.account_id
    assert identifier1.code == identifier2.code


def test_account_identifier_individiual_uniqueness(account_identifier):
    for attribute in ["last_name", "date_of_birth"]:
        AccountIdentifierFactory(**{attribute: getattr(account_identifier, attribute)})


def test_account_identifier_with_null_code(account_identifier):
    with pytest.raises(IntegrityError):
        AccountIdentifierFactory(account_id=account_identifier.account_id, code=None)
