import random

import pytest
from django.db import IntegrityError

from activate_account.factories import AccountFactory
from activate_account.models import Account

pytestmark = pytest.mark.django_db


def test_account_separate_uniqueness():
    n_accounts = random.randrange(10, 21)

    for _ in range(n_accounts):
        AccountFactory()

    assert Account.objects.count() == n_accounts


def test_account_crsid_case():
    factory_account = AccountFactory(crsid="ABC123")
    account = Account.objects.get(pk="abc123")
    factory_account.refresh_from_db()
    assert account == factory_account

    # Check that creating a CRSId with different casing raises an integrity error
    with pytest.raises(IntegrityError):
        AccountFactory(crsid="aBc123")
