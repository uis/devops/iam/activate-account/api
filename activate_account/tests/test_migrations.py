import pytest
from django.db import DEFAULT_DB_ALIAS, connections
from django.db.migrations.exceptions import IrreversibleError
from django.db.migrations.executor import MigrationExecutor


@pytest.mark.django_db
def test_reversible_migrations():
    db_alias = DEFAULT_DB_ALIAS
    connection = connections[db_alias]
    executor = MigrationExecutor(connection)

    app_label = "activate_account"

    all_migrations = executor.loader.graph.leaf_nodes(app_label)
    all_migrations = sorted(
        all_migrations, key=lambda m: list(executor.loader.graph.forwards_plan(m))
    )

    for i in range(1, len(all_migrations)):
        target = all_migrations[i]
        prev_migration = all_migrations[i - 1]

        # Apply the migration
        executor.migrate([target])

        # Reverse the migration
        try:
            executor.migrate([prev_migration])
        except IrreversibleError as ie:
            pytest.fail(
                f"Migration {target} to {prev_migration} is not reversible. "
                f"IrreversibleError: {ie}"
            )
        except Exception as e:
            pytest.fail(
                f"Unexpected error when reversing migration {target} to {prev_migration}. "
                f"Error: {e}"
            )
