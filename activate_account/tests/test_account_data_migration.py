import pytest

from activate_account.apps import Config


@pytest.mark.django_db
def test_account_data_migration(migrator):
    migrator = migrator()
    app_name = Config.name
    prev_migration = "0005_remove_account_unique_account_account_created_at_and_more"
    target_migration = "0006_accountidentifier_remove_account_unique_account_and_more"

    migrator.migrate(app_name, prev_migration)
    Account = migrator.apps.get_model(app_name, "Account")
    Account.objects.create(
        crsid="testcrsid",
        last_name="testlastname",
        date_of_birth="1996-05-07",
        code="testcode",
        valid_at="1996-05-07",
    )
    migrator.migrate(app_name, target_migration)

    Account = migrator.apps.get_model(app_name, "Account")

    assert Account.objects.filter(crsid="testcrsid").exists()
    AccountIdentifier = migrator.apps.get_model(app_name, "AccountIdentifier")
    assert AccountIdentifier.objects.filter(last_name="testlastname").exists()
    AccountIdentifier.objects.create(
        account_id=Account.objects.get(crsid="testcrsid"),
        last_name="testlastname",
        date_of_birth="1996-05-07",
        code="testcode2",
    )

    migrator.migrate(app_name, prev_migration)
    Account = migrator.apps.get_model(app_name, "Account")

    assert Account.objects.filter(
        crsid="testcrsid", last_name="testlastname", code="testcode"
    ).exists()
