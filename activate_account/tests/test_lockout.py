import pytest
from django.db import IntegrityError, transaction
from django.utils import timezone

from activate_account.factories import LockoutFactory
from activate_account.models import Lockout

pytestmark = pytest.mark.django_db


def test_lockout_create():
    lockout = LockoutFactory()
    assert Lockout.objects.count() == 1
    assert lockout.attempts == 0
    assert lockout.lockout_until is None


def test_lockout_create_locked_out():
    lockout = LockoutFactory(locked_out=True)
    assert Lockout.objects.count() == 1
    assert lockout.attempts > 0
    assert lockout.lockout_until is not None
    assert lockout.lockout_until > timezone.now()


def test_lockout_unique_constraint():
    LockoutFactory(identity_key="user1", date_of_birth="2000-01-01")
    with pytest.raises(IntegrityError):
        with transaction.atomic():
            LockoutFactory(identity_key="user1", date_of_birth="2000-01-01")

    LockoutFactory(identity_key="user2", date_of_birth="2000-01-01")
    LockoutFactory(identity_key="user1", date_of_birth="2001-02-02")
