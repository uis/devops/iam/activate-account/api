import random

import factory
from django.utils import timezone

from .models import Account, AccountDetails, AccountIdentifier, Lockout
from .reset_tokens import PasswordAppTokenResponse


class AccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Account

    valid_at = factory.LazyFunction(timezone.now)

    @factory.lazy_attribute
    def crsid(self):
        # Choose 'abcdejmnrst' as the first letter for CRSid because these are the most
        # commonly used initials in names, which helps minimize the risk of infinite loops
        # in the name matching process within the AccountDetailsFactory.
        return (
            f"{random.choice('abcdejmnrst')}{random.choice('abcdejmnrst')}"
            f"{random.randint(199, 99999)}"
        )

    class Params:
        account_details = factory.Trait(
            details=factory.RelatedFactory(
                "activate_account.factories.AccountDetailsFactory", factory_related_name="account"
            )
        )
        account_identifier = factory.Trait(
            identifier=factory.RelatedFactory(
                "activate_account.factories.AccountIdentifierFactory",
                factory_related_name="account_id",
            )
        )


class AccountDetailsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AccountDetails

    account = factory.SubFactory(AccountFactory)
    college = factory.Faker("company")
    affiliation = factory.Faker("company")
    terms_accepted = False

    @factory.lazy_attribute
    def name(self):
        # Generate full name
        faker = factory.Faker._get_faker()
        first_letter = self.account.crsid[0].upper()
        second_letter = self.account.crsid[1].upper()
        last_name = faker.last_name()

        attempts = 0
        max_attempts = 20
        while not last_name.startswith(second_letter) and attempts < max_attempts:
            last_name = faker.last_name()
            attempts += 1
        title = faker.prefix().replace(".", " ") if faker.boolean() else ""

        return f"{last_name} {title} {first_letter}."


class AccountIdentifierFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AccountIdentifier

    account_id = factory.SubFactory(AccountFactory)
    date_of_birth = factory.Faker("date_of_birth")

    @factory.lazy_attribute
    def last_name(self):
        # Ideally we'd duplicate what is in AccountDetails but not all Accounts have one
        faker = factory.Faker._get_faker()
        first_letter = self.account_id.crsid[0].upper()
        second_letter = self.account_id.crsid[1].upper()
        last_name = faker.last_name()

        attempts = 0
        max_attempts = 20
        while not last_name.startswith(second_letter) and attempts < max_attempts:
            last_name = faker.last_name()
            attempts += 1

        return f"{last_name.upper()} {first_letter}"

    @factory.lazy_attribute
    def code(self):
        code_type = random.choice(["staff", "postgraduate", "pgce", "undergraduate"])
        faker = factory.Faker._get_faker()
        if code_type == "staff":
            return faker.bothify("???###?##").lower()
        elif code_type == "postgraduate":
            return faker.numerify(text="3########")
        elif code_type == "pgce":
            return faker.numerify(text="#######")
        elif code_type == "undergraduate":
            return faker.numerify(text="##########")


# Factory Boy doesn't support dataclasses, so do our own thing
class PasswordAppTokenResponseFactory:
    def __new__(cls):
        faker = factory.Faker._get_faker()
        return PasswordAppTokenResponse(
            token=faker.hexify(text="^^^^-^^^^-^^^^-^^^^", upper=True),
        )


class LockoutFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Lockout

    identity_key = factory.Sequence(lambda n: f"user{n}")
    date_of_birth = factory.Faker("date_of_birth")
    attempts = 0
    lockout_until = None

    class Params:
        locked_out = factory.Trait(
            attempts=5,
            lockout_until=factory.LazyFunction(
                lambda: timezone.now() + timezone.timedelta(minutes=10)
            ),
        )
