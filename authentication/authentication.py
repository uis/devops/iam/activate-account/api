from django.utils import timezone
from drf_spectacular.extensions import OpenApiAuthenticationExtension
from knox.auth import TokenAuthentication as KnoxTokenAuthentication
from rest_framework import exceptions


class TokenAuthenticationExtension(OpenApiAuthenticationExtension):
    target_class = "authentication.authentication.TokenAuthentication"
    name = "TokenAuth"

    def get_security_definition(self, auto_schema):
        return {"type": "http", "scheme": "bearer"}


class TokenAuthentication(KnoxTokenAuthentication):
    def validate_user(self, auth_token):
        """
        We override this function as we don't use the user model and therefore don't check for
        auth_token.user.is_active
        """
        return (auth_token.user, auth_token)

    def authenticate_credentials(self, token):
        try:
            return super().authenticate_credentials(token)
        except exceptions.AuthenticationFailed:
            raise exceptions.AuthenticationFailed({"error": "invalid_token"})

    def _cleanup_token(self, auth_token):
        """
        We override this function as we don't use the user model. This function expects the
        `AuthToken.user` model to have a `get_username` method which is used as an argument to the
        `token_expired` signal. The logic below is the same as `super()._cleanup_token`, except
        for sending the signal.
        """
        for other_token in auth_token.user.auth_token_set.all():
            if other_token.digest != auth_token.digest and other_token.expiry:
                if other_token.expiry < timezone.now():
                    other_token.delete()

        if auth_token.expiry is not None:
            if auth_token.expiry < timezone.now():
                auth_token.delete()
                return True

        return False
