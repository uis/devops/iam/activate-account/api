SESSION_GRANT_TYPE = "urn:devops.uis.cam.ac.uk:params:oauth:grant-type:new-user-credentials"
LOCKOUT_ATTEMPTS = 3
LOCKOUT_INITIAL_SECONDS = 10
