import re
import unicodedata


def normalise_name(name):
    """
    Performs some changes to the given name to provide something that can be used when for
    comparison when attempting to authenticate with a last name. Simulates (some of) the
    behaviour that jackdaw signup processes use.
    """
    # Replace some special characters (most importantly hyphens) with spaces
    make_spaces = [
        # hyphen-like characters
        "-",
        "\u2010",
        "\u2013",
        "\u2014",
        "\u2015",
        # other characters jackdaw replaces with spaces
        "?",
        "*",
        ".",
        "%",
        "_",
    ]
    for char in make_spaces:
        name = name.replace(char, " ")

    # Multiple whitespaces to a single space
    name = re.sub(r"\s{2,}", " ", name)

    # Jackdaw only has ascii/latin characters so we convert diacritics to their base character
    name = unicodedata.normalize("NFKD", name).encode("ascii", "ignore").decode("utf8")

    # Jackdaw also converts some vowels couples to a single character, but we aren't sure we want
    # to do that here.
    # i.e. "AA" with "A", "AE" with "A", "OE" with "O", "UE" with "U"

    # Remove leading and trailing whitespaces
    return name.strip().upper()
