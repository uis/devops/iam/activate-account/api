from django.conf import settings
from django.urls import path
from rest_framework import routers

from .views import LockoutViewSet, LoginView, LogoutAllView, LogoutView

urlpatterns = [
    path("token/", LoginView.as_view(), name="knox_login"),
    path("token/revoke/", LogoutView.as_view(), name="knox_logout"),
    path("token/revoke/all/", LogoutAllView.as_view(), name="knox_logoutall"),
]

if settings.INTERNAL_API_ENABLED:
    router = routers.SimpleRouter()
    router.register("lockout", LockoutViewSet, basename="lockout")
    urlpatterns += router.urls
