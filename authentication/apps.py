from django.apps import AppConfig


class Config(AppConfig):
    name = "authentication"
    verbose_name = "Authentication"
