import structlog
from drf_spectacular.utils import OpenApiResponse, extend_schema
from knox.views import LoginView as KnoxLoginView
from rest_framework import (
    mixins,
    parsers,
    renderers,
    serializers,
    status,
    views,
    viewsets,
)
from rest_framework.response import Response

from activate_account.models import Lockout
from authentication.errors import OAuth2Error
from authentication.serializers import (
    EmptySerializer,
    LockoutSerializer,
    TokenErrorSerializer,
    TokenRequestSerializer,
    TokenResponseSerializer,
)

logger = structlog.get_logger(__name__)


class LoginView(KnoxLoginView):
    throttle_classes = ()
    permission_classes = ()
    authentication_classes = ()
    parser_classes = (parsers.FormParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = TokenRequestSerializer
    versioning_class = None

    def get_serializer_context(self):
        return {"request": self.request, "format": self.format_kwarg, "view": self}

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        return self.serializer_class(*args, **kwargs)

    @extend_schema(
        summary="Authenticate a User",
        description="Authenticates a user and returns an access token.",
        request=TokenRequestSerializer,
        responses={
            200: TokenResponseSerializer,
            400: OpenApiResponse(
                response=TokenErrorSerializer,
                description=(
                    "Invalid request, such as both or none of crsid and last name are provided, "
                    "or no matching user."
                ),
            ),
        },
        tags=["authentication"],
        operation_id="getToken",
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # KnoxLoginView expects the user to be set on the request object
        self.request.user = serializer.validated_data["account"]

        # Below is copied from KnoxLoginView. Note that we can't use super().post directly because
        # it sends the `user_logged_in` signal. In our case, because we have not explicitly set a
        # `AUTH_USER_MODEL` and the default django User model has a `last_login` field, that means
        # that Django listens with the `update_last_login` on `user_logged_in` which would fail if
        # our `Account` model would be passed as an instance to `user_logged_in` because it
        # doesn't have a `last_login` field.
        #
        # Note that we left out the logic for `token_limit_per_user` as we are not using this
        # functionality.
        _, token = self.create_token()

        return Response(
            data={
                "expires_in": int(self.get_token_ttl().total_seconds()),
                "access_token": token,
                "token_type": "bearer",
            },
            headers={
                "Cache-Control": "no-store",
                "Pragma": "no-cache",
            },
        )

    def get_exception_handler(self):
        default_handler = super().get_exception_handler()

        def exception_handler(exc, context):
            if isinstance(exc, serializers.ValidationError):
                return Response({"error": "invalid_request"}, status=status.HTTP_400_BAD_REQUEST)

            if isinstance(exc, OAuth2Error):
                return Response(
                    {"error": exc.error, "error_description": exc.error_description},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            return default_handler(exc, context)

        return exception_handler


class LogoutView(views.APIView):
    throttle_classes = ()
    versioning_class = None
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = EmptySerializer

    def get_post_response(self, request):
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @extend_schema(
        tags=["authentication"],
        summary="Log out User",
        description="Logs out the current logged-in user by revoking their authentication token.",
        responses={
            204: OpenApiResponse(
                response=EmptySerializer,
                description="Successfully logged out, no content to return.",
            ),
            401: OpenApiResponse(
                response=TokenErrorSerializer,
                description="Unauthorized request, possibly due to an invalid token.",
            ),
        },
        methods=["POST"],
        operation_id="revokeTokenSession",
    )
    def post(self, request, format=None):
        request._auth.delete()
        return self.get_post_response(request)


class LogoutAllView(views.APIView):
    throttle_classes = ()
    versioning_class = None
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = EmptySerializer

    def get_post_response(self, request):
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @extend_schema(
        tags=["authentication"],
        summary="Log out from All Sessions",
        description="Logs out the current logged-in user from all sessions by revoking all their "
        "authentication tokens.",
        responses={
            204: OpenApiResponse(
                response=EmptySerializer,
                description="Successfully logged out from all sessions, no content to return.",
            ),
            401: OpenApiResponse(
                response=TokenErrorSerializer,
                description="Unauthorized request, possibly due to an invalid token.",
            ),
        },
        methods=["POST"],
        operation_id="revokeAllTokenSessions",
    )
    def post(self, request, format=None):
        request.user.auth_token_set.all().delete()
        return self.get_post_response(request)


class LockoutViewSet(
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = ()
    authentication_classes = ()
    queryset = Lockout.objects.all()
    serializer_class = LockoutSerializer
    versioning_class = None

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        logger.info(
            "Lockout removed",
            identity_key=instance.identity_key,
            date_of_birth=instance.date_of_birth,
            category="audit",
        )
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
