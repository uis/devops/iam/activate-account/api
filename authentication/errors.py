from rest_framework import exceptions
from rest_framework.views import exception_handler as drf_exception_handler


class OAuth2Error(RuntimeError):
    pass


# `UnsupportedGrantTypeError` and `InvalidGrantError` cannot be derived from
# `exceptions.ValidationError` because the serializer would wrap them in a
# new `ValidationError` in `to_internal_value` and `run_validation`, losing information
# about the original error class.
class InvalidGrantError(OAuth2Error):
    error = "invalid_grant"
    error_description = "The authentication credentials provided could not be verified"


class UnsupportedGrantTypeError(OAuth2Error):
    error = "unsupported_grant_type"
    error_description = "This server does not support the provided grant type"


class TokenNotAuthenticatedError(exceptions.NotAuthenticated):
    auth_header = "Bearer"
    default_detail = {"error": "invalid_token"}


def exception_handler(exc, context):
    if isinstance(exc, exceptions.NotAuthenticated):
        exc = TokenNotAuthenticatedError()

    return drf_exception_handler(exc, context)
