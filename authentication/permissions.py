from rest_framework import permissions

from activate_account.models import Account


class IsAuthenticatedWithAccount(permissions.BasePermission):
    def has_permission(self, request, view):
        return isinstance(request.user, Account)
