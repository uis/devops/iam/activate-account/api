from django.db import models
from knox.models import AbstractAuthToken

from activate_account.models import Account


class AuthToken(AbstractAuthToken):
    user = models.ForeignKey(
        Account, null=False, blank=False, on_delete=models.CASCADE, related_name="auth_token_set"
    )
