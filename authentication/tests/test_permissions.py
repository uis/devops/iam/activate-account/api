import pytest
from django.contrib.auth import get_user_model
from rest_framework import status, views
from rest_framework.response import Response

from authentication.permissions import IsAuthenticatedWithAccount

pytestmark = pytest.mark.django_db


@pytest.fixture
def view():
    class TestView(views.APIView):
        versioning_class = None

        def get(self, request):
            return Response(status=status.HTTP_200_OK)

    return TestView.as_view()


@pytest.fixture
def user(faker):
    return get_user_model()._default_manager.create(username=faker.user_name())


def assert_is_authorized(response):
    assert response.status_code == status.HTTP_200_OK


def assert_is_unauthorized(response):
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.data == {"error": "invalid_token"}


def test_default_permission_classes_unauthorized(view, rf):
    """DEFAULT_PERMISSION_CLASSES to always include IsAuthenticatedWithAccount"""
    assert view.cls.permission_classes == [IsAuthenticatedWithAccount]

    assert_is_unauthorized(view(rf.get("/")))


def test_allow_account_model(view, account, rf):
    request = rf.get("/")
    request.user = account
    assert IsAuthenticatedWithAccount().has_permission(request, view)


def test_refuse_user_model(view, user, rf):
    request = rf.get("/")
    request.user = user
    assert not IsAuthenticatedWithAccount().has_permission(request, view)
