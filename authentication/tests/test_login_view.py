import random
from string import ascii_uppercase
from unittest import mock
from urllib.parse import urlencode

import pytest
from django.urls import reverse
from knox.settings import knox_settings
from rest_framework import exceptions, parsers, status

from activate_account.factories import AccountFactory, AccountIdentifierFactory
from authentication.constants import SESSION_GRANT_TYPE
from authentication.serializers import TokenRequestSerializer

pytestmark = pytest.mark.django_db


@pytest.fixture
def valid_session_grant_body(request, account_identifier):
    account = account_identifier.account_id
    code = account_identifier.code
    date_of_birth = account_identifier.date_of_birth
    last_name = account_identifier.last_name
    if getattr(request, "param", "crsid") == "crsid":
        return {
            "grant_type": SESSION_GRANT_TYPE,
            "crsid": account.crsid,
            "code": code,
            "date_of_birth": date_of_birth.strftime("%Y-%m-%d"),
        }
    else:
        return {
            "grant_type": SESSION_GRANT_TYPE,
            "last_name": last_name,
            "code": code,
            "date_of_birth": date_of_birth.strftime("%Y-%m-%d"),
        }


def post_login(
    client, body, *, content_type="application/x-www-form-urlencoded", process_data=urlencode
):
    return client.post(reverse("knox_login"), data=process_data(body), content_type=content_type)


def assert_is_error(response, expected_error, status_code=status.HTTP_400_BAD_REQUEST):
    assert response.status_code == status_code
    assert response.json()["error"] == expected_error


def assert_is_valid_login(response):
    assert response.status_code == status.HTTP_200_OK

    assert response["Cache-Control"] == "no-store"
    assert response["Pragma"] == "no-cache"

    data = response.json()

    assert "access_token" in data
    assert data["token_type"] == "bearer"
    assert data["expires_in"] == knox_settings.TOKEN_TTL.total_seconds()


@pytest.mark.parametrize("valid_session_grant_body", ["crsid", "last_name"], indirect=True)
def test_unsupported_grant_type(client, valid_session_grant_body):
    """grant_type must be set to a supported value"""
    assert_is_error(
        post_login(client, {**valid_session_grant_body, "grant_type": "invalid"}),
        "unsupported_grant_type",
    )


@pytest.mark.parametrize("valid_session_grant_body", ["crsid", "last_name"], indirect=True)
@pytest.mark.parametrize("parser", [parsers.JSONParser, parsers.MultiPartParser])
def test_unsupported_media_type_multipart(client, valid_session_grant_body, parser):
    """content_type must be application/x-www-form-urlencoded"""
    media_type = parser.media_type
    response = client.post(
        reverse("knox_login"), json=valid_session_grant_body, content_type=media_type
    )
    assert response.status_code == status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
    assert response.data == {"detail": f'Unsupported media type "{media_type}" in request.'}


@pytest.mark.parametrize("valid_session_grant_body", ["crsid", "last_name"], indirect=True)
def test_valid_session_grant(client, valid_session_grant_body, caplog):
    """A valid session grant request body succeeds"""
    assert_is_valid_login(post_login(client, valid_session_grant_body))
    # Confirm that the audit log entry was made
    assert "Successful authentication" in caplog.text
    assert "'category': 'audit'" in caplog.text


@pytest.mark.parametrize("valid_session_grant_body", ["crsid"], indirect=True)
def test_valid_crsid_case_insensitive(client, valid_session_grant_body):
    """CRSId comparison is case insensitive"""
    field = "crsid"
    assert_is_valid_login(
        post_login(
            client,
            {
                **valid_session_grant_body,
                field: valid_session_grant_body[field].upper(),  # Upper case as fixture is lower
            },
        )
    )


@pytest.mark.parametrize("valid_session_grant_body", ["last_name"], indirect=True)
def test_valid_last_name_case_insensitive(client, valid_session_grant_body):
    """Last name comparison is case insensitive (due to normalisation)"""
    field = "last_name"
    assert_is_valid_login(
        post_login(
            client,
            {
                **valid_session_grant_body,
                field: valid_session_grant_body[field].lower(),  # Lower case as fixture is upper
            },
        )
    )


@pytest.mark.parametrize("valid_session_grant_body", ["last_name"], indirect=True)
def test_valid_last_name_partial(client, valid_session_grant_body, faker):
    """Last name comparison also matches partial (starting followed by space)"""
    field = "last_name"
    # Just the first part with random case
    just_first_part = valid_session_grant_body[field].split(" ")[0].lower()
    just_first_part = "".join(
        just_first_part[i].upper() if faker.boolean() else just_first_part[i]
        for i in range(len(just_first_part))
    )
    assert_is_valid_login(
        post_login(
            client,
            {
                **valid_session_grant_body,
                field: just_first_part,
            },
        )
    )


@pytest.mark.parametrize("valid_session_grant_body", ["last_name"], indirect=True)
def test_valid_last_name_partial_multiple(client, valid_session_grant_body, account_identifier):
    """Last name partial comparison could potentially match multiple accounts"""
    just_first_part = account_identifier.last_name.split(" ")[0]
    # Create another account identifier with the same date of birth, code and first part of last
    # name (but different full last name)
    while True:
        alt_last_name = f"{just_first_part} {random.choice(ascii_uppercase)}"
        if alt_last_name != account_identifier.last_name:
            break
    alt_account = AccountFactory()
    AccountIdentifierFactory(
        account_id=alt_account,
        last_name=alt_last_name,
        date_of_birth=account_identifier.date_of_birth,
        code=account_identifier.code,
    )
    assert_is_error(
        post_login(
            client,
            {
                **valid_session_grant_body,
                "last_name": just_first_part,
            },
        ),
        "invalid_grant",
    )


@pytest.mark.parametrize("valid_session_grant_body", ["crsid", "last_name"], indirect=True)
def test_missing_field(client, request, valid_session_grant_body):
    """Missing a required field results in 'invalid_request'"""
    missing_field_body = {**valid_session_grant_body}
    del missing_field_body[request.node.callspec.params["valid_session_grant_body"]]
    assert_is_error(post_login(client, missing_field_body), "invalid_request")


def test_invalid_grant(faker, client, caplog):
    """Providing credentials that do not exist"""
    assert_is_error(
        post_login(
            client,
            {
                "grant_type": SESSION_GRANT_TYPE,
                "last_name": faker.last_name(),
                "code": faker.slug(),
                "date_of_birth": faker.date(),
            },
        ),
        "invalid_grant",
    )
    # Confirm that the audit log entry was made
    assert "No matching user" in caplog.text
    assert "'category': 'audit'" in caplog.text


@pytest.mark.parametrize("valid_session_grant_body", ["crsid", "last_name"], indirect=True)
def test_code_must_match(faker, client, valid_session_grant_body):
    """code must match expected value"""
    assert_is_error(
        post_login(client, {**valid_session_grant_body, "code": faker.slug()}), "invalid_grant"
    )


def test_default_exception_handler(client, valid_session_grant_body):
    """Default exception handler handles all other errors"""
    with mock.patch.object(
        TokenRequestSerializer, "validate", side_effect=exceptions.APIException
    ):
        response = post_login(client, valid_session_grant_body)

    assert response.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR
    assert response.json() == {"detail": "A server error occurred."}
