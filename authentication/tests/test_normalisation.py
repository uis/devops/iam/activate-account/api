import pytest

from authentication.normalisation import normalise_name

EXPECTED_NORMALISATION = [
    # Fairly common "anglicized" names
    ("Morgan A.", "MORGAN A"),
    ("O'Leary P.B.", "O'LEARY P B"),
    ("von Neumann J.", "VON NEUMANN J"),
    # Diacritics and hyphens
    ("Müller-Platz H.", "MULLER PLATZ H"),
    ("Ångström A.", "ANGSTROM A"),
    ("König M.A.", "KONIG M A"),
    ("Hernández H.P.", "HERNANDEZ H P"),
    ("Crête-Lafrenière P.", "CRETE LAFRENIERE P"),
    ("Saint\u2013Martin Dr J.", "SAINT MARTIN DR J"),
    # Some characters are just lost (not good)
    ("Hermeß    M.", "HERME M"),
    ("Æsir B.", "SIR B"),
    ("Bjørnson Prof. G.", "BJRNSON PROF G"),
    # Nonsense
    ("Bad?Data****Entered_%Here", "BAD DATA ENTERED HERE"),
]


@pytest.mark.parametrize("unnormalised,normalised", EXPECTED_NORMALISATION)
def test_normalise_name(unnormalised, normalised):
    assert normalise_name(unnormalised) == normalised
