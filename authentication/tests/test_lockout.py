from datetime import timedelta
from urllib.parse import urlencode

import pytest
from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time

from activate_account.factories import AccountFactory, LockoutFactory
from activate_account.models import Lockout
from authentication.constants import (
    LOCKOUT_ATTEMPTS,
    LOCKOUT_INITIAL_SECONDS,
    SESSION_GRANT_TYPE,
)
from authentication.errors import InvalidGrantError
from authentication.serializers import LockoutSerializer, TokenRequestSerializer

pytestmark = pytest.mark.django_db


def _build_data(identity_field, identity_value, identifier):
    data = {
        "grant_type": SESSION_GRANT_TYPE,
        "date_of_birth": identifier.date_of_birth.strftime("%Y-%m-%d"),
        "code": identifier.code,
    }
    data[identity_field] = identity_value
    return data


@pytest.mark.parametrize("identity_field", ["crsid", "last_name"])
def test_lockout_existing_active(identity_field):
    account = AccountFactory(account_identifier=True)
    identifier = account.account_identifier.first()
    identity_value = account.crsid if identity_field == "crsid" else identifier.last_name

    future_time = timezone.now() + timedelta(minutes=5)
    LockoutFactory(
        identity_key=identity_value,
        date_of_birth=identifier.date_of_birth,
        attempts=LOCKOUT_ATTEMPTS,
        lockout_until=future_time,
    )

    data = _build_data(identity_field, identity_value, identifier)
    serializer = TokenRequestSerializer(data=data)
    with pytest.raises(InvalidGrantError, match="Account locked out"):
        serializer.is_valid(raise_exception=True)


@pytest.mark.parametrize("identity_field", ["crsid", "last_name"])
def test_lockout_created_on_invalid_credentials(identity_field):
    account = AccountFactory(account_identifier=True)
    identifier = account.account_identifier.first()
    identity_value = account.crsid if identity_field == "crsid" else identifier.last_name

    data = _build_data(identity_field, identity_value, identifier)
    data["code"] = "wrong-code"

    serializer = TokenRequestSerializer(data=data)
    with pytest.raises(InvalidGrantError, match="No matching account"):
        serializer.is_valid(raise_exception=True)

    lockout = Lockout.objects.filter(
        identity_key=identity_value, date_of_birth=identifier.date_of_birth
    ).first()
    assert lockout is not None
    assert lockout.attempts == 1
    assert lockout.lockout_until is None


@pytest.mark.parametrize("identity_field", ["crsid", "last_name"])
def test_successful_login_clears_lockout(identity_field):
    account = AccountFactory(account_identifier=True)
    identifier = account.account_identifier.first()
    identity_value = account.crsid if identity_field == "crsid" else identifier.last_name

    past_time = timezone.now() - timedelta(minutes=1)
    LockoutFactory(
        identity_key=identity_value,
        date_of_birth=identifier.date_of_birth,
        attempts=3,
        lockout_until=past_time,
    )

    data = _build_data(identity_field, identity_value, identifier)
    serializer = TokenRequestSerializer(data=data)
    serializer.is_valid(raise_exception=True)
    assert "account" in serializer.validated_data
    assert (
        Lockout.objects.filter(
            identity_key=identity_value, date_of_birth=identifier.date_of_birth
        ).first()
        is None
    )


@pytest.mark.parametrize("identity_field", ["crsid", "last_name"])
def test_exponential_backoff_lockout(identity_field):
    with freeze_time("2025-01-01 00:00:00") as frozen_datetime:
        account = AccountFactory(account_identifier=True)
        identifier = account.account_identifier.first()
        identity_value = account.crsid if identity_field == "crsid" else identifier.last_name

        data = _build_data(identity_field, identity_value, identifier)
        data["code"] = "wrong-code"

        for i in range(LOCKOUT_ATTEMPTS):
            serializer = TokenRequestSerializer(data=data)
            with pytest.raises(InvalidGrantError, match="No matching account"):
                serializer.is_valid(raise_exception=True)
            lockout = Lockout.objects.filter(
                identity_key=identity_value, date_of_birth=identifier.date_of_birth
            ).first()
            if i < LOCKOUT_ATTEMPTS - 1:
                assert lockout.lockout_until is None
            else:
                expected_duration = LOCKOUT_INITIAL_SECONDS * (2 ** (i + 1 - LOCKOUT_ATTEMPTS))
                expected_lockout_time = timezone.now() + timedelta(seconds=expected_duration)
                assert (lockout.lockout_until - expected_lockout_time).total_seconds() == 0

        frozen_datetime.move_to(lockout.lockout_until + timedelta(seconds=1))

        serializer = TokenRequestSerializer(data=data)
        with pytest.raises(InvalidGrantError, match="No matching account"):
            serializer.is_valid(raise_exception=True)
        lockout = Lockout.objects.filter(
            identity_key=identity_value, date_of_birth=identifier.date_of_birth
        ).first()
        expected_duration = LOCKOUT_INITIAL_SECONDS * (2 ** (lockout.attempts - LOCKOUT_ATTEMPTS))
        expected_lockout_time = timezone.now() + timedelta(seconds=expected_duration)
        assert (lockout.lockout_until - expected_lockout_time).total_seconds() == 0


@pytest.mark.parametrize("identity_field", ["crsid", "last_name"])
@freeze_time("2025-01-01 00:00:00")
def test_lockout_clear_from_api_endpoint_flow(identity_field, api_client, caplog):
    account = AccountFactory(account_identifier=True)
    identifier = account.account_identifier.first()
    identity_value = account.crsid if identity_field == "crsid" else identifier.last_name

    data = _build_data(identity_field, identity_value, identifier)
    wrong_data = {
        **data,
        "code": "wrong-code",
    }

    # Fail LOCK_ATTEMPTS times
    for _ in range(LOCKOUT_ATTEMPTS):
        response = api_client.post(
            reverse("knox_login"),
            data=urlencode(wrong_data),
            content_type="application/x-www-form-urlencoded",
        )

        assert response.status_code == 400

    lockout_kwargs = {
        "identity_key": identity_value,
        "date_of_birth": identifier.date_of_birth,
    }

    # Confirm account is locked out
    assert Lockout.objects.filter(**lockout_kwargs).exists()

    caplog.clear()
    response = api_client.post(
        reverse("knox_login"),
        data=urlencode(data),
        content_type="application/x-www-form-urlencoded",
    )
    # Confirm that the audit log entry was made
    assert "Account locked out" in caplog.text
    assert "'category': 'audit'" in caplog.text

    assert response.status_code == 400

    lockout = Lockout.objects.get(**lockout_kwargs)

    response = api_client.get(reverse("lockout-list"))
    assert response.status_code == 200
    assert response.data == LockoutSerializer([lockout], many=True).data

    # Now remove the lockout
    response = api_client.delete(reverse("lockout-detail", args=[lockout.id]))
    assert response.status_code == 204

    # And confirm it's gone
    assert not Lockout.objects.filter(**lockout_kwargs).exists()

    # And that we can login again
    response = api_client.post(
        reverse("knox_login"),
        data=urlencode(data),
        content_type="application/x-www-form-urlencoded",
    )

    assert response.status_code == 200
