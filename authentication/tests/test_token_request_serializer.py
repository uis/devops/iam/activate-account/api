import pytest

from authentication.constants import SESSION_GRANT_TYPE
from authentication.errors import InvalidGrantError
from authentication.serializers import TokenRequestSerializer


def test_mutual_exclusive():
    data = {
        "grant_type": SESSION_GRANT_TYPE,
        "date_of_birth": "1980-03-25",
        "crsid": "ab1234",
        "last_name": "Smith",
        "code": "ABCDEF123456",
    }
    serializer = TokenRequestSerializer(data=data)
    assert not serializer.is_valid()
    assert serializer.errors == {
        "non_field_errors": ["At most one of crsid and last name may be provided"]
    }


def test_missing_crsid_and_last_name():
    data = {
        "grant_type": SESSION_GRANT_TYPE,
        "date_of_birth": "1980-03-25",
        "code": "ABCDEF123456",
    }
    serializer = TokenRequestSerializer(data=data)
    assert not serializer.is_valid()
    assert serializer.errors == {
        "non_field_errors": ["At least one of crsid and last name must be provided"]
    }


@pytest.mark.django_db
def test_empty_post_normalise_last_name():
    data = {
        "grant_type": SESSION_GRANT_TYPE,
        "date_of_birth": "1980-03-25",
        "last_name": "-?_",
        "code": "ABCDEF123456",
    }
    serializer = TokenRequestSerializer(data=data)
    # Fails with "no matching account" before checking the database
    with pytest.raises(InvalidGrantError) as excinfo:
        serializer.is_valid()
    assert str(excinfo.value) == "No matching account"
