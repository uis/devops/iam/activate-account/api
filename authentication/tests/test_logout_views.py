import random

import pytest
from django.urls import reverse
from rest_framework import status

from authentication.models import AuthToken
from authentication.tests.factories import AuthTokenFactory


def assert_is_unauthorized(response):
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.parametrize(
    "viewname,revoked_all", [("knox_logout", False), ("knox_logoutall", True)]
)
def test_logout(authenticated_api_client, account, viewname, revoked_all):
    n_tokens = random.randrange(10, 21)
    AuthTokenFactory.create_batch(n_tokens, user=account)

    # Also create some tokens that are not for this user
    n_other_tokens = random.randrange(10, 21)
    AuthTokenFactory.create_batch(n_other_tokens)

    # authenticated_api_client also created an authentication token for the user
    assert account.auth_token_set.count() == n_tokens + 1

    url = reverse(viewname)

    response = authenticated_api_client.post(url)

    assert response.status_code == status.HTTP_204_NO_CONTENT

    expected_count = 0 if revoked_all else n_tokens
    assert account.auth_token_set.count() == expected_count

    # Tokens from other users should not be touched
    assert AuthToken.objects.exclude(user=account).count() == n_other_tokens

    # We are unauthorised now, so trying to logout again will fail even while we provide an access
    # token.
    assert_is_unauthorized(authenticated_api_client.post(url))


@pytest.mark.parametrize("viewname", ["knox_logout", "knox_logoutall"])
def test_logout_unauthenticated(api_client, viewname):
    assert_is_unauthorized(api_client.post(reverse(viewname)))
