import factory
from knox.settings import knox_settings

from activate_account.factories import AccountFactory
from authentication.models import AuthToken


class AuthTokenFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AuthToken

    user = factory.SubFactory(AccountFactory, account_identifier=True, account_details=True)
    prefix = knox_settings.TOKEN_PREFIX
    expiry = knox_settings.TOKEN_TTL
