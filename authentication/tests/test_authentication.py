from datetime import timedelta
from unittest import mock

import pytest
from django.urls import reverse
from knox.settings import knox_settings
from rest_framework import status

from activate_account.factories import AccountFactory
from authentication.models import AuthToken
from authentication.tests.factories import AuthTokenFactory

pytestmark = pytest.mark.django_db


@pytest.fixture
def authentication_test_url():
    return reverse("v1alpha1:account")


def test_unauthenticated_version_view(api_client, authentication_test_url):
    response = api_client.get(authentication_test_url, format="json")

    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.data == {"error": "invalid_token"}


@pytest.fixture
def get_login_response(api_client, authentication_test_url):
    def login(token):
        return api_client.get(
            authentication_test_url, format="json", headers={"Authorization": f"Bearer {token}"}
        )

    return login


def test_wrong_bearer_token(get_login_response):
    response = get_login_response("invalidToken")

    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.data == {"error": "invalid_token"}


def test_expired_other_token_cleanup(get_login_response):
    account = AccountFactory(account_identifier=True)
    expired_token_instance, _ = AuthTokenFactory(user=account, expiry=timedelta(days=-1))
    _, token = AuthTokenFactory(user=account)

    response = get_login_response(token)

    assert response.status_code == status.HTTP_200_OK

    # Check if the token does not exist anymore
    assert not AuthToken.objects.filter(pk=expired_token_instance.pk).exists()


@pytest.mark.parametrize(
    "delta,status_code,exists",
    [
        (
            knox_settings.TOKEN_TTL - timedelta(minutes=1),  # 19 minutes
            status.HTTP_200_OK,
            True,
        ),
        (
            knox_settings.TOKEN_TTL,  # 20 minutes
            status.HTTP_200_OK,
            True,
        ),
        (
            knox_settings.TOKEN_TTL + timedelta(minutes=1),
            status.HTTP_401_UNAUTHORIZED,
            False,
        ),  # 21 minutes
    ],
)
def test_expired_current_token_cleanup(delta, status_code, exists, get_login_response):
    """TOKEN_TTL limits the time between two consecutive requests"""
    token_instance, token = AuthTokenFactory()

    # We can't use `token_instance.created` here, because
    # `token_instance.created + knox_settings.TOKEN_TTL` is not _exactly_ equal to
    # `token_instance.created`. This is because `expiry` is passed in as a `timedelta`
    # object and not a `datetime.datetime` object.
    created = token_instance.expiry - knox_settings.TOKEN_TTL
    mock_time = created + delta

    # To prevent flaky tests and timing issues, we mock the time to a specific value
    with mock.patch("django.utils.timezone.now", return_value=mock_time):
        response = get_login_response(token)

    assert response.status_code == status_code

    # Check if the token exists or not
    assert AuthToken.objects.filter(pk=token_instance.pk).exists() is exists


@pytest.mark.parametrize(
    "delta",
    [
        knox_settings.AUTO_REFRESH_MAX_TTL - timedelta(minutes=1),  # 29 minutes
        knox_settings.AUTO_REFRESH_MAX_TTL,  # 30 minutes
        knox_settings.AUTO_REFRESH_MAX_TTL + timedelta(minutes=1),  # 31 minutes
    ],
)
def test_renew_token(delta, get_login_response):
    """AUTO_REFRESH_MAX_TTL limits allowable token lifetime"""
    token_instance, token = AuthTokenFactory()

    # We can't use `token_instance.created` here, because
    # `token_instance.created + knox_settings.TOKEN_TTL` is not _exactly_ equal to
    # `token_instance.created`. This is because `expiry` is passed in as a `timedelta`
    # object and not a `datetime.datetime` object.
    created = token_instance.expiry - knox_settings.TOKEN_TTL

    # This is the projected new expiry time if we would ignore `knox_settings.AUTO_REFRESH_MAX_TTL`
    projected_expiry_time = created + delta

    # We travel to the time `knox_settings.TOKEN_TTL` before the `projected_expiry_time`
    mock_time = projected_expiry_time - knox_settings.TOKEN_TTL

    # And we set the expiry to be on the time we try log in to make sure that that the token is
    # still valid
    token_instance.expiry = mock_time
    token_instance.save(update_fields=["expiry"])

    # To prevent flaky tests and timing issues, we mock the time to a specific value
    with mock.patch("django.utils.timezone.now", return_value=mock_time):
        response = get_login_response(token)

    # We should be able to login and the token should exist
    assert response.status_code == status.HTTP_200_OK
    assert AuthToken.objects.filter(pk=token_instance.pk).exists()

    token_instance.refresh_from_db()

    # And now we make sure that the expiry time does not exceed the
    # `knox_settings.AUTO_REFRESH_MAX_TTL`
    assert token_instance.expiry <= token_instance.created + knox_settings.AUTO_REFRESH_MAX_TTL

    # But is higher than we initially set (confirming it was auto-renewed)
    assert token_instance.expiry > mock_time
