from datetime import timedelta

import structlog
from django.utils import timezone
from rest_framework import serializers

from activate_account.models import Account, Lockout
from authentication.constants import (
    LOCKOUT_ATTEMPTS,
    LOCKOUT_INITIAL_SECONDS,
    SESSION_GRANT_TYPE,
)
from authentication.errors import InvalidGrantError, UnsupportedGrantTypeError
from authentication.normalisation import normalise_name

logger = structlog.get_logger(__name__)


class TokenResponseSerializer(serializers.Serializer):
    """
    Defines the structure of a successful token response.
    """

    expires_in = serializers.IntegerField()
    access_token = serializers.CharField()
    token_type = serializers.CharField()


class TokenRequestSerializer(serializers.Serializer):
    """
    Represents the data required to request a token.
    """

    grant_type = serializers.CharField()
    crsid = serializers.CharField(required=False, allow_blank=True)
    last_name = serializers.CharField(required=False, allow_blank=True)
    date_of_birth = serializers.DateField(format="%Y-%m-%d")
    code = serializers.CharField()

    def validate_grant_type(self, grant_type) -> str:
        """
        Validates the grant type provided in the token request.

        This method checks if the provided grant type matches the SESSION_GRANT_TYPE. If not,
        it raises an UnsupportedGrantTypeError.

        Args:
            grant_type (str): The grant type specified in the request.

        Returns:
            str: The validated grant type if it matches the expected type.

        Raises:
            UnsupportedGrantTypeError: If the provided grant type is not supported.
        """

        if grant_type != SESSION_GRANT_TYPE:
            raise UnsupportedGrantTypeError("Unsupported grant type")

        return grant_type

    def validate(self, data) -> dict:
        """
        Validates the entire data set of the token request.

        This method performs checks to ensure that exactly one of the identification fields
        (crsid or last name) is provided and that an account exists that matches the provided
        credentials including the 'date_of_birth' and 'code'. If the validations pass, the account
        object is included in the validated data returned.

        Args:
            data (dict): The complete data of the token request to be validated.

        Returns:
            dict: The original data dictionary with an added 'account' key that holds the Account
            object.

        Raises:
            serializers.ValidationError: If both or neither of the identification fields (crsid,
                last name) are provided.
            InvalidGrantError: If no matching account is found based on the provided credentials.
        """

        if "crsid" in data and "last_name" in data:
            raise serializers.ValidationError("At most one of crsid and last name may be provided")
        if "crsid" not in data and "last_name" not in data:
            raise serializers.ValidationError(
                "At least one of crsid and last name must be provided"
            )
        identity_key = data.get("crsid") or data.get("last_name")
        date_of_birth = data["date_of_birth"]

        lockout = Lockout.objects.filter(
            identity_key=identity_key, date_of_birth=date_of_birth
        ).first()

        if lockout and lockout.lockout_until and timezone.now() < lockout.lockout_until:
            logger.warn(
                "Account locked out",
                identity_key=identity_key,
                date_of_birth=date_of_birth,
                lockout_until=lockout.lockout_until,
                category="audit",
            )
            raise InvalidGrantError("Account locked out")

        name_match = None
        try:
            if "crsid" in data:
                account = Account.objects.get(
                    crsid=data["crsid"],
                    account_identifier__date_of_birth=data["date_of_birth"],
                    account_identifier__code=data["code"],
                )
            else:
                name_match = normalise_name(data["last_name"])
                if not name_match:
                    # If we don't have anything to compare, treat as not found
                    raise Account.DoesNotExist
                try:
                    # first match last_name fields starting with name_match followed by a space
                    account = Account.objects.get(
                        account_identifier__last_name__istartswith=f"{name_match} ",
                        account_identifier__date_of_birth=data["date_of_birth"],
                        account_identifier__code=data["code"],
                    )
                except Account.MultipleObjectsReturned:
                    logger.warn(
                        "Multiple accounts found with normalised last name",
                        last_name=data.get("last_name"),
                        date_of_birth=data["date_of_birth"],
                        name_match=name_match,
                        category="audit",
                    )
                    raise InvalidGrantError("Ambiguous token request")
                except Account.DoesNotExist:
                    # then match last_name fields being just name_match
                    account = Account.objects.get(
                        account_identifier__last_name=name_match,
                        account_identifier__date_of_birth=data["date_of_birth"],
                        account_identifier__code=data["code"],
                    )
        except Account.DoesNotExist:
            if lockout:
                lockout.attempts += 1
            else:
                lockout = Lockout(
                    identity_key=identity_key, date_of_birth=date_of_birth, attempts=1
                )

            if lockout.attempts >= LOCKOUT_ATTEMPTS:
                lockout_duration = LOCKOUT_INITIAL_SECONDS * (
                    2 ** (lockout.attempts - LOCKOUT_ATTEMPTS)
                )
                lockout.lockout_until = timezone.now() + timedelta(seconds=lockout_duration)
            lockout.save()

            logger.warn(
                "No matching user in token request.",
                crsid=data.get("crsid"),
                last_name=data.get("last_name"),
                date_of_birth=data.get("date_of_birth"),
                name_match=name_match,
                category="audit",
            )
            raise InvalidGrantError("No matching account")

        if lockout:
            lockout.delete()

        # Set account in validated data
        data["account"] = account

        logger.info(
            "Successful authentication",
            crsid=data.get("crsid"),
            last_name=data.get("last_name"),
            date_of_birth=data.get("date_of_birth"),
            name_match=name_match,
            account=account.crsid,
            category="audit",
        )

        return data


class TokenErrorSerializer(serializers.Serializer):
    """
    OAuth2-compatible error response returned from the token endpoint when a token could not be
    issued.
    """

    error = serializers.ChoiceField(
        choices=[
            (
                "unsupported_grant_type",
                "The authorization grant type is not supported by the authorization server.",
            ),
            (
                "invalid_request",
                "The request is missing a required parameter, includes an unsupported parameter "
                "value or is otherwise malformed.",
            ),
            ("invalid_grant", "The provided credentials do not match any known user"),
        ]
    )
    error_description = serializers.CharField(required=False)
    error_uri = serializers.URLField(required=False)


class LockoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lockout
        fields = ("id", "identity_key", "date_of_birth", "attempts", "lockout_until")


class EmptySerializer(serializers.Serializer):
    """
    Defines an empty response.
    """

    pass
