---
title: Session authentication
---

# Session authentication through the token API

In order to leverage existing tooling for OAuth2, it was decided that retrieving the access token
and handling authentication would follow the OAuth2 specification. This means we have an API
endpoint available on `/token/` that accepts a application/x-www-form-urlencoded encoded document
POST-ed to it with the following fields:

* `grant_type` - must be `urn:devops.uis.cam.ac.uk:params:oauth:grant-type:new-user-credentials`
* `crsid` - must be the CRSid as a string
* `date_of_birth` - must be a RFC 3339 formatted date. E.g. 1996-12-19. No time-portion is accepted.
* `last_name` - must be a string
* `code` - must be a string

Either `crsid` or `last_name` must be provided, but not both.

An example request would be (line breaks added for display purposes):

```
POST /token HTTP/1.1
Host: server.example.com
Content-Type: application/x-www-form-urlencoded

grant_type=urn%3Adevops.uis.cam.ac.uk%3Aparams%3Aoauth%3Agrant-type
%3Anew-user-credentials&date_of_birth=1996-12-19&last_name=Smith&co
de=ABCDEF12345
```

If `grant_type` is anything other than what we expect, we respond with a 400 response as per [the
spec](https://datatracker.ietf.org/doc/html/rfc6749#section-5.2) with the following content:

```
{
  "error": "unsupported_grant_type"
}
```

If the user credentials are missing a required field, e.g. no `code` when it is required, we respond
with a 400 response and:

```
{
  "error": "invalid_request"
}
```

If the all the required user credentials are provided but they do not match a known user, we respond
with a 400 response and the following document:

```
{
  "error": "invalid_grant"
}
```

If all goes well, we return a [RFC 6749](https://datatracker.ietf.org/doc/html/rfc6749) compliant
JSON document response. The `access_token` field is the newly minted token, the `token_type` field
must be `bearer` and the `expires_in` field gives the lifetime of the token in seconds. An example
response:

```
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache

{
  "access_token":"abcdefg1234567",
  "token_type":"bearer",
  "expires_in":3600,
}
```

To authenticate to other API endpoints, we would set the following header: `Authorization: Bearer
abcdefg1234567`.
