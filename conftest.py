import os
from urllib.parse import urlencode

import pytest
from django.conf import settings
from django.db import connection
from django.db.migrations.executor import MigrationExecutor
from django.urls import reverse
from pytest_docker_tools import container
from rest_framework.test import APIClient

from activate_account.factories import (
    AccountDetailsFactory,
    AccountFactory,
    AccountIdentifierFactory,
    PasswordAppTokenResponseFactory,
)
from authentication.constants import SESSION_GRANT_TYPE

# We only modify the database settings for the test suite if TEST_USE_EXTERNAL_DATABASE is not set
# to a non-empty value.
if os.environ.get("TEST_USE_EXTERNAL_DATABASE", "") == "":
    postgres_container = container(
        scope="session",
        image="postgres",
        environment={
            "POSTGRES_USER": "pytest-user",
            "POSTGRES_PASSWORD": "pytest-pass",
            "POSTGRES_DB": "pytest-db",
        },
        ports={
            "5432/tcp": None,
        },
    )

    @pytest.fixture(scope="session")
    def django_db_modify_db_settings(
        django_db_modify_db_settings_parallel_suffix, postgres_container
    ):
        """
        Modify database settings based on the postgres docker container we spun up.
        """
        host, port = postgres_container.get_addr("5432/tcp")
        db_settings = {
            "HOST": host,
            "PORT": port,
            "NAME": "pytest-db",
            "USER": "pytest-user",
            "PASSWORD": "pytest-pass",
        }

        for db_item in settings.DATABASES.values():
            db_item.update(db_settings)
            db_item.get("TEST", {}).update(db_settings)


class Migrator:
    def __init__(self, connection=connection):
        self.executor = MigrationExecutor(connection)

    def migrate(self, app_label: str, migration: str):
        target = [(app_label, migration)]
        self.executor.loader.build_graph()
        self.executor.migrate(target)
        self.apps = self.executor.loader.project_state(target).apps


@pytest.fixture
def migrator():
    return Migrator


@pytest.fixture
def account(db):
    return AccountFactory()


@pytest.fixture
def account_details(account):
    return AccountDetailsFactory(account=account)


@pytest.fixture
def account_identifier(account):
    return AccountIdentifierFactory(account_id=account)


@pytest.fixture
def reset_token():
    return PasswordAppTokenResponseFactory()


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def authenticated_api_client(account, account_identifier, api_client):
    response = api_client.post(
        reverse("knox_login"),
        data=urlencode(
            {
                "grant_type": SESSION_GRANT_TYPE,
                "crsid": account.crsid,
                "code": account_identifier.code,
                "date_of_birth": account_identifier.date_of_birth.strftime("%Y-%m-%d"),
            }
        ),
        content_type="application/x-www-form-urlencoded",
    )
    return APIClient(headers={"Authorization": f"Bearer {response.json()['access_token']}"})
