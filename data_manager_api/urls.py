from rest_framework import routers

from data_manager_api.views import AccountViewSet

router = routers.DefaultRouter()
router.register("accounts", AccountViewSet, "account")

urlpatterns = router.urls
