import structlog
from django.conf import settings
from django.db import IntegrityError
from django.http import Http404
from django.utils import timezone
from drf_spectacular.utils import OpenApiResponse, extend_schema
from rest_framework import status, viewsets
from rest_framework.response import Response

from activate_account.models import Account
from data_manager_api.serializers import AccountDataSerializer, ValidAtSerializer

logger = structlog.get_logger(__name__)


class ExpiredEventError(Exception):
    pass


@extend_schema(
    summary="Update account data",
    description=(
        "Provides a way for external services to update the account data in the application. Note "
        "that this is API endpoint is private and for internal use only. This endpoint will not "
        "exist in the public API."
    ),
    tags=["internal account management"],
    parameters=[ValidAtSerializer],
)
class AccountViewSet(viewsets.GenericViewSet):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = AccountDataSerializer
    queryset = Account.all_objects.all()
    versioning_class = None
    lookup_url_kwarg = "crsid"

    def get_valid_at(self):
        serializer = ValidAtSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)

        return serializer.validated_data["valid_at"]

    def check_valid_at(self, obj, valid_at):
        if valid_at <= obj.valid_at:
            raise ExpiredEventError()

    @extend_schema(
        responses={
            status.HTTP_202_ACCEPTED: OpenApiResponse(
                response=None,
                description="Event is outdated and is ignored.",
            ),
            status.HTTP_204_NO_CONTENT: OpenApiResponse(
                response=None,
                description="Successful deletion of account data.",
            ),
        },
    )
    def destroy(self, request, *args, **kwargs):
        valid_at = self.get_valid_at()

        if settings.DATA_MANAGER_READ_ONLY:
            logger.info("read_only_delete", request=request)
            return Response(status=status.HTTP_204_NO_CONTENT)

        try:
            instance = self.get_object()
        except Http404:
            # No instance found, so apparently the first event we receive for this account is a
            # delete event. That means we still want to create an instance, and set the deleted_at
            # and valid_at, so we can use this for potential following events.
            instance = Account.objects.create(
                crsid=self.kwargs["crsid"],
                deleted_at=timezone.now(),
                valid_at=valid_at,
            )
        else:
            self.check_valid_at(instance, valid_at)

            instance.deleted_at = timezone.now()
            instance.valid_at = valid_at
            instance.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    @extend_schema(
        responses={
            status.HTTP_200_OK: OpenApiResponse(
                response=AccountDataSerializer,
                description="Successful update of account data.",
            ),
            status.HTTP_201_CREATED: OpenApiResponse(
                response=AccountDataSerializer,
                description="Successful creation of account data.",
            ),
            status.HTTP_202_ACCEPTED: OpenApiResponse(
                response=None,
                description="Event is outdated and is ignored.",
            ),
        },
    )
    def update(self, request, *args, **kwargs):
        """
        We only allow a single endpoint to update the data, both for creation and updating. This
        method is basically combining what mixins.CreateModelMixin and mixins.UpdateModelMixin
        would do, except for the fact that we're only allowing PUT, and not POST or PATCH.
        """
        valid_at = self.get_valid_at()

        instance = None

        try:
            instance = self.get_object()
        except Http404:
            # No instance found, so we know we want to create a new instance.
            pass
        else:
            self.check_valid_at(instance, valid_at)

        serializer = self.get_serializer(instance=instance, data=request.data, valid_at=valid_at)
        serializer.is_valid(raise_exception=True)

        # In read-only mode, do not save the model
        if settings.DATA_MANAGER_READ_ONLY:
            logger.info("read_only_update", request=request)
        else:
            # The serializer calls `update` when it has an instance passed in, while it calls
            # `create` when it has no instance passed in.
            try:
                serializer.save()
            except IntegrityError:
                return Response(
                    {"error": "Duplicate identifier details provided."},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            if instance and getattr(instance, "_prefetched_objects_cache", None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

        # If instance is not set on the serializer, attempting to call `serializer.data` will
        # result in an exception, as the instance object passed to the to_representation call will
        # be a dictionary.
        # The instance will be set either if one exists in the database, or after a call to
        # serializer.save.
        response_data = serializer.data if serializer.instance else serializer.initial_data
        return Response(
            response_data, status=status.HTTP_200_OK if instance else status.HTTP_201_CREATED
        )

    def get_exception_handler(self):
        default_handler = super().get_exception_handler()

        def exception_handler(exc, context):
            if isinstance(exc, ExpiredEventError):
                return Response(status=status.HTTP_202_ACCEPTED)

            return default_handler(exc, context)

        return exception_handler
