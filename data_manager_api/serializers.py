from django.db import transaction
from rest_framework import serializers

from activate_account.models import Account, AccountDetails, AccountIdentifier
from authentication.normalisation import normalise_name


class ValidAtSerializer(serializers.Serializer):
    valid_at = serializers.DateTimeField()


class AccountDataSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source="account_details.name", default="")
    affiliation = serializers.CharField(source="account_details.affiliation", default="")
    college = serializers.CharField(source="account_details.college", default="")
    codes = serializers.ListField(
        child=serializers.CharField(max_length=100),
        required=True,
        allow_empty=False,
        write_only=True,
    )
    last_name = serializers.CharField(write_only=True, required=False)
    date_of_birth = serializers.DateField(write_only=True)

    class Meta:
        model = Account
        fields = (
            "crsid",
            "last_name",
            "date_of_birth",
            "codes",
            "name",
            "affiliation",
            "college",
        )

    def to_representation(self, instance):
        """Properly serialize codes from related AccountCode entries"""
        representation = super().to_representation(instance)

        identifiers = instance.account_identifier.all()
        if identifiers:
            representation["codes"] = [code.code for code in identifiers]
            representation["last_name"] = identifiers[0].last_name
            representation["date_of_birth"] = identifiers[0].date_of_birth.strftime("%Y-%m-%d")

        return representation

    def __init__(self, *args, **kwargs):
        self.valid_at = kwargs.pop("valid_at", None)
        super().__init__(*args, **kwargs)

    @transaction.atomic
    def create(self, validated_data):
        codes = validated_data.pop("codes", [])
        account_details = validated_data.pop("account_details")
        # Calculate last_name from normalising full name
        last_name = normalise_name(account_details.get("name", ""))
        # Remove last_name if present in request
        validated_data.pop("last_name", None)
        date_of_birth = validated_data.pop("date_of_birth")
        account = Account.objects.create(valid_at=self.valid_at, **validated_data)
        AccountDetails.objects.create(
            account=account,
            **account_details,
        )
        for code in codes:
            AccountIdentifier.objects.create(
                account_id=account,
                code=code,
                last_name=last_name,
                date_of_birth=date_of_birth,
            )

        return account

    @transaction.atomic
    def update(self, instance, validated_data):
        codes = validated_data.pop("codes", [])
        account_details_data = validated_data.pop("account_details")
        # Calculate last_name from normalising full name
        last_name = normalise_name(account_details_data.get("name", ""))
        # Remove last_name if present in request
        validated_data.pop("last_name", None)

        date_of_birth = validated_data.pop("date_of_birth")

        instance.account_identifier.all().delete()

        # Update Account
        for key, value in validated_data.items():
            setattr(instance, key, value)

        instance.valid_at = self.valid_at

        # Explicitly set `deleted_at` to `None` to undo any soft-deletions
        instance.deleted_at = None

        instance.save()

        # Create new AccountIdentifier instances
        for code in codes:
            AccountIdentifier.objects.create(
                account_id=instance,
                code=code,
                last_name=last_name,
                date_of_birth=date_of_birth,
            )

        if hasattr(instance, "account_details"):
            # Update AccountDetails
            for key, value in account_details_data.items():
                setattr(instance.account_details, key, value)

            instance.account_details.save()
        else:
            AccountDetails.objects.create(account=instance, **account_details_data)

        return instance
