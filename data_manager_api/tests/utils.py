from activate_account.factories import (
    AccountDetailsFactory,
    AccountFactory,
    AccountIdentifierFactory,
)


def data_account_factory(account=None, account_details=None, account_identifier=None):
    account = account or AccountFactory.build()
    account_details = account_details or AccountDetailsFactory.build(account=account)
    account_identifier = account_identifier or AccountIdentifierFactory.build(account_id=account)

    return {
        "crsid": account.crsid,
        "name": account_details.name,
        "college": account_details.college,
        "affiliation": account_details.affiliation,
        "codes": [account_identifier.code],
        "date_of_birth": account_identifier.date_of_birth,
    }
