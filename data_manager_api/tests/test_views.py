from datetime import timedelta
from urllib.parse import urlencode

import pytest
from django.urls import reverse
from django.utils import timezone
from rest_framework import status

from activate_account.factories import (
    AccountDetailsFactory,
    AccountFactory,
    AccountIdentifierFactory,
)
from activate_account.models import Account, AccountIdentifier
from authentication.normalisation import normalise_name
from data_manager_api.tests.utils import data_account_factory

pytestmark = pytest.mark.django_db


def build_account_detail_url(crsid, valid_at):
    url = reverse("data_manager_api:account-detail", kwargs={"crsid": crsid})
    query_params = {"valid_at": valid_at}
    return f"{url}?{urlencode(query_params)}"


def test_account_creation(api_client):
    account = AccountFactory.build()

    data = data_account_factory(account)

    crsid = account.crsid
    valid_at = account.valid_at

    # Sanity-check: no record in DB
    assert not Account.objects.filter(crsid=crsid).exists()
    assert not AccountIdentifier.objects.filter(account_id=account).exists()

    # We try to update while the object does not exist yet
    response = api_client.put(build_account_detail_url(crsid, valid_at), data)

    # We instead created the object
    assert response.status_code == status.HTTP_201_CREATED

    assert Account.objects.filter(crsid=crsid).exists()


def test_account_details_post_creation(api_client):
    account = AccountFactory.build()
    data = data_account_factory(account)
    crsid = account.crsid
    valid_at = account.valid_at

    api_client.put(build_account_detail_url(crsid, valid_at), data)

    # Fetch the saved account
    saved_account = Account.objects.get(crsid=crsid)

    assert saved_account.crsid == data["crsid"]
    # last_name is a normalised version of the name, last_name in the request is ignored
    assert saved_account.account_identifier.first().last_name == normalise_name(data["name"])
    assert saved_account.account_identifier.first().date_of_birth == data["date_of_birth"]

    saved_codes = {code.code for code in saved_account.account_identifier.all()}
    input_codes = set(data["codes"])
    assert saved_codes == input_codes

    assert saved_account.account_details.name == data["name"]
    assert saved_account.account_details.college == data["college"]
    assert saved_account.account_details.affiliation == data["affiliation"]


def test_account_update_codes(api_client):
    account = AccountFactory(account_details=True, account_identifier=True)
    data = data_account_factory(account)

    updated_code = "new_code2"
    data["codes"] = [updated_code]
    valid_at = timezone.now().isoformat()

    response = api_client.put(build_account_detail_url(account.crsid, valid_at), data)
    assert response.status_code == status.HTTP_200_OK
    assert response.data["codes"] == [updated_code]

    saved_account = Account.objects.get(crsid=account.crsid)
    saved_codes = {code.code for code in saved_account.account_identifier.all()}
    assert saved_codes == {updated_code}


def test_account_update(api_client):
    account = AccountFactory(account_details=True, account_identifier=True)
    account_details = account.account_details
    valid_at = account.valid_at + timedelta(hours=1)

    initial_data = data_account_factory(account)
    new_data = {
        "name": "Ryu Min-seok",
        "codes": ["new_code"],
        "last_name": "Ignored",  # No longer required
        "date_of_birth": "1996-05-07",
    }

    assert account_details.name != new_data["name"]  # Sanity check

    response = api_client.put(
        build_account_detail_url(account.crsid, valid_at),
        {**initial_data, **new_data},
    )

    assert response.status_code == status.HTTP_200_OK

    account.refresh_from_db()
    account.account_identifier.all().first().refresh_from_db()

    assert Account.objects.filter(
        crsid=account.crsid, account_details__name=new_data["name"]
    ).exists()

    # last_name is a normalised version of the name
    assert account.account_identifier.all().first().last_name == normalise_name(new_data["name"])
    assert (
        account.account_identifier.all().first().date_of_birth.strftime("%Y-%m-%d")
        == new_data["date_of_birth"]
    )
    assert account.account_identifier.all().first().code == new_data["codes"][0]


def test_account_update_valid_at_earlier(api_client):
    account = AccountFactory(account_details=True, account_identifier=True)
    account_details = account.account_details
    account_identifier = account.account_identifier.first()

    # Update event was supposed to come before the event that we processed already
    valid_at = account.valid_at - timedelta(hours=1)

    initial_data = data_account_factory(account, account_details, account_identifier)
    new_data = {
        "name": "Lee Min-hyung",
        "codes": ["new_code"],
        "last_name": "Ignored",
        "date_of_birth": "1996-05-07",
    }

    assert account_details.name != new_data["name"]  # Sanity check
    assert account_identifier.last_name != new_data["last_name"]  # Sanity check

    response = api_client.put(
        build_account_detail_url(account.crsid, valid_at),
        {**initial_data, **new_data},
    )

    assert response.status_code == status.HTTP_202_ACCEPTED

    # Name not updated, same as initially
    assert Account.objects.filter(
        crsid=account.crsid, account_details__name=account.account_details.name
    ).exists()


def assert_is_soft_deleted(crsid):
    assert not Account.objects.filter(crsid=crsid).exists()
    assert Account.all_objects.filter(crsid=crsid, deleted_at__isnull=False).exists()


def test_account_delete(api_client):
    account_details = AccountDetailsFactory()
    instance = account_details.account

    valid_at = instance.valid_at + timedelta(hours=1)

    response = api_client.delete(build_account_detail_url(instance.crsid, valid_at))

    assert response.status_code == status.HTTP_204_NO_CONTENT

    # We soft-deleted the object
    assert_is_soft_deleted(instance.crsid)


def test_account_delete_while_did_not_exist_before(api_client):
    crsid = "ab1234"
    assert not Account.objects.filter(crsid=crsid).exists()  # Sanity check

    valid_at = timezone.now()

    response = api_client.delete(build_account_detail_url(crsid, valid_at))

    assert response.status_code == status.HTTP_204_NO_CONTENT

    # We actually created a soft-deleted object
    assert_is_soft_deleted(crsid)


def test_account_delete_valid_at_earlier(api_client):
    account_details = AccountDetailsFactory()
    instance = account_details.account

    # Delete event was supposed to come before create event
    valid_at = instance.valid_at - timedelta(hours=1)

    response = api_client.delete(build_account_detail_url(instance.crsid, valid_at))

    assert response.status_code == status.HTTP_202_ACCEPTED

    # User still exists, we ignored the event
    assert Account.objects.filter(crsid=instance.crsid, deleted_at=None).exists()


def test_read_only_mode_delete(api_client, settings):
    settings.DATA_MANAGER_READ_ONLY = True

    account = AccountFactory(account_details=True, account_identifier=True)
    valid_at = account.valid_at + timedelta(hours=1)

    response = api_client.delete(
        build_account_detail_url(account.crsid, valid_at),
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert Account.objects.filter(crsid=account.crsid, deleted_at=None).exists()

    response = api_client.delete(build_account_detail_url("notpresent", valid_at))

    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert Account.objects.filter(crsid=account.crsid, deleted_at=None).exists()


def test_read_only_mode_update(api_client, settings):
    settings.DATA_MANAGER_READ_ONLY = True

    account = AccountFactory(account_details=True, account_identifier=True)
    account_details = account.account_details
    account_identifier = account.account_identifier.first()
    valid_at = account.valid_at + timedelta(hours=1)

    initial_data = data_account_factory(account, account_details, account_identifier)
    new_data = {
        "name": "Ryu Min-seok",
        "codes": ["new_code"],
        "last_name": "Ignored",
        "date_of_birth": "1996-05-07",
    }

    assert account_details.name != new_data["name"]  # Sanity check
    assert account_identifier.last_name != new_data["last_name"]

    response = api_client.put(
        build_account_detail_url(account.crsid, valid_at),
        {**initial_data, **new_data},
    )

    assert response.status_code == status.HTTP_200_OK
    assert Account.objects.get(crsid=account.crsid).account_details.name == initial_data["name"]


def test_read_only_mode_create(api_client, settings):
    settings.DATA_MANAGER_READ_ONLY = True

    account = AccountFactory.build()

    data = data_account_factory(account)

    crsid = account.crsid
    valid_at = account.valid_at

    # Sanity-check: no record in DB
    assert not Account.objects.filter(crsid=crsid).exists()
    assert not AccountIdentifier.objects.filter(account_id=account).exists()

    # We try to update while the object does not exist yet
    response = api_client.put(build_account_detail_url(crsid, valid_at), data)

    # We instead created the object
    assert response.status_code == status.HTTP_201_CREATED
    assert not Account.objects.filter(crsid=crsid).exists()


@pytest.mark.parametrize("method", ("put", "delete"))
def test_account_valid_at_missing(api_client, method):
    account_details = AccountDetailsFactory()
    instance = account_details.account

    func = getattr(api_client, method)
    response = func(reverse("data_manager_api:account-detail", kwargs={"crsid": instance.crsid}))

    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_account_delete_then_update(api_client):
    account = AccountFactory.build()
    data = data_account_factory(account)

    # Sanity check
    assert not Account.objects.filter(crsid=account.crsid).exists()

    # First delete the account
    response = api_client.delete(
        build_account_detail_url(account.crsid, timezone.now()),
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT

    assert_is_soft_deleted(account.crsid)

    # And then send an update
    response = api_client.put(build_account_detail_url(account.crsid, timezone.now()), data)

    assert response.status_code == status.HTTP_200_OK

    # Account is undeleted
    assert Account.objects.filter(crsid=account.crsid, deleted_at=None).exists()
    # last_name is a normalised version of the name
    assert Account.objects.get(
        crsid=account.crsid
    ).account_identifier.first().last_name == normalise_name(data["name"])


def test_account_multiple_deletes(api_client):
    crsid = "ab1234"

    now = timezone.now()

    response = api_client.delete(
        build_account_detail_url(crsid, now - timedelta(hours=1)),
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert_is_soft_deleted(crsid)

    instance = Account.all_objects.get(crsid=crsid)

    # Delete it again
    response = api_client.delete(
        build_account_detail_url(crsid, timezone.now()),
    )

    # Still soft-deleted
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert_is_soft_deleted(crsid)

    # And `deleted_at` is updated
    assert Account.all_objects.filter(
        crsid=instance.crsid, deleted_at__gte=instance.deleted_at
    ).exists()


def test_unique_identifier_information(api_client):
    account = AccountFactory.build()
    account_details = AccountDetailsFactory.build(account=account)
    account_identifier = AccountIdentifierFactory.build(
        account_id=account, last_name=normalise_name(account_details.name)
    )

    response = api_client.put(
        build_account_detail_url(account.crsid, timezone.now()),
        data_account_factory(account, account_details, account_identifier),
    )

    assert response.status_code == status.HTTP_201_CREATED
    assert Account.objects.filter(crsid=account.crsid).exists()

    # Build another account with the same name in the account details (as this is the source of
    # the last_name of the account identifier)
    other_account = AccountFactory.build()
    account_details = AccountDetailsFactory.build(account=other_account, name=account_details.name)

    assert account.crsid != other_account.crsid  # Sanity check

    response = api_client.put(
        build_account_detail_url(other_account.crsid, timezone.now()),
        data_account_factory(other_account, account_details, account_identifier),
    )

    assert response.status_code != status.HTTP_201_CREATED
    assert not Account.objects.filter(crsid=other_account.crsid).exists()
