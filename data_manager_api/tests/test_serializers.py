import pytest
from rest_framework.exceptions import ValidationError

from activate_account.factories import AccountFactory
from activate_account.models import Account, AccountIdentifier
from authentication.normalisation import normalise_name
from data_manager_api.serializers import AccountDataSerializer
from data_manager_api.tests.utils import data_account_factory

pytestmark = pytest.mark.django_db


def test_create():
    account = AccountFactory.build()
    data = data_account_factory(account)

    codes = data["codes"]
    name = data["name"]
    college = data["college"]
    affiliation = data["affiliation"]
    dob = data["date_of_birth"]
    valid_at = account.valid_at

    assert not Account.objects.filter(crsid=data["crsid"]).exists()  # Sanity check
    assert not AccountIdentifier.objects.filter(code=codes[0]).exists()

    serializer = AccountDataSerializer(data=data, valid_at=valid_at)
    serializer.is_valid(raise_exception=True)

    instance = serializer.save()

    assert instance.crsid == account.crsid

    account_identifiers = instance.account_identifier.all()
    first_account_identifier = account_identifiers.first()
    assert first_account_identifier.code == codes[0]
    assert first_account_identifier.date_of_birth == dob
    assert first_account_identifier.last_name == normalise_name(name)

    assert instance.account_details.name == name
    assert instance.account_details.college == college
    assert instance.account_details.affiliation == affiliation


@pytest.mark.parametrize("partial", [True, False])
def test_update(partial):
    account = AccountFactory(account_identifier=True)

    initial_data = data_account_factory(account)
    new_data = {
        "name": "Lee Sang-hyeok",
        "last_name": "Ignored",
        "date_of_birth": "1996-05-07",
        "codes": ["testcode123"],
    }
    assert account.account_details.name != new_data["name"]  # Sanity check

    valid_at = account.valid_at

    serializer = AccountDataSerializer(
        instance=account,
        partial=partial,
        data=new_data if partial else {**initial_data, **new_data},
        valid_at=valid_at,
    )
    serializer.is_valid()

    instance = serializer.save()

    assert instance.account_details.name == new_data["name"]
    assert instance.account_identifier.first().last_name == normalise_name(new_data["name"])


def test_override_nullable_fields():
    serializer = AccountDataSerializer(
        data={
            "crsid": "ab1234",
        },
    )

    with pytest.raises(ValidationError):
        serializer.is_valid(raise_exception=True)


def test_update_removes_old_codes():
    account = AccountFactory(account_identifier=True, account_details=True)
    old_code = account.account_identifier.first().code
    new_codes = ["new_code1", "new_code2"]

    data = data_account_factory(account)
    data["codes"] = new_codes

    serializer = AccountDataSerializer(instance=account, data=data, valid_at=account.valid_at)
    serializer.is_valid(raise_exception=True)
    serializer.save()

    account.refresh_from_db()
    current_codes = {code.code for code in account.account_identifier.all()}
    assert current_codes == set(new_codes)
    assert not AccountIdentifier.objects.filter(code=old_code).exists()


def test_codes_empty_validation():
    data = {
        "crsid": "ab1234",
        "last_name": "Ignored",
        "date_of_birth": "2000-01-01",
        "codes": [],
        "name": "Doe J.",
        "affiliation": "Student",
        "college": "College",
    }
    serializer = AccountDataSerializer(data=data)
    with pytest.raises(ValidationError) as excinfo:
        serializer.is_valid(raise_exception=True)
    assert "codes" in excinfo.value.detail
    assert "This list may not be empty." in str(excinfo.value.detail["codes"])
