from django.urls import reverse
from rest_framework import status


def test_version_view_is_correct(authenticated_api_client):
    """
    Ensure that the available versions are displayed at the root uri
    """
    url = reverse("available-versions")
    response = authenticated_api_client.get(url, format="json")

    # ensure that our versions view is the root view
    assert url == "/"

    # ensure we get the expected response
    assert response.status_code == status.HTTP_200_OK
    assert response.data == {"v1alpha1": "http://testserver/v1alpha1/"}

    # ensure that all the urls for the different versions return a success response code
    for url in response.data.values():
        response = authenticated_api_client.get(url, format="json")
        assert response.status_code == status.HTTP_200_OK
