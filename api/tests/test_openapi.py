from io import StringIO

import pytest
import yaml
from django.core.management import call_command
from openapi_spec_validator import validate
from openapi_spec_validator.validation.exceptions import OpenAPIValidationError


def test_openapi_schema():
    """
    Test OpenAPI schema generation and validate it against the OpenAPI specification.
    """
    try:
        with StringIO() as output:
            # Run the Django management command that generates the OpenAPI schema
            call_command("spectacular", stdout=output)

            schema_data = yaml.safe_load(output.getvalue())

        assert "openapi" in schema_data
        assert "paths" in schema_data

        validate(schema_data)
    except OpenAPIValidationError as e:
        pytest.fail(f"OpenAPI schema validation failed: {e}")
