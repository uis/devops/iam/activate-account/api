from rest_framework import serializers

from activate_account.models import Account, AccountDetails, AccountIdentifier


class AccountSerializer(serializers.ModelSerializer):
    """
    Provides account details, including name, affiliation, college, and terms acceptance status.
    """

    name = serializers.CharField(source="account_details.name", default="")
    affiliation = serializers.CharField(source="account_details.affiliation", default="")
    college = serializers.CharField(source="account_details.college", default="")
    terms_accepted = serializers.BooleanField(
        source="account_details.terms_accepted", default=False
    )
    date_of_birth = serializers.DateField(
        source="account_identifier.date_of_birth", allow_null=True, required=False
    )

    def update(self, instance, validated_data) -> Account:
        """
        Updates the terms_accepted status of an account's related AccountDetails instance.

        Args:
            instance (Account): The account instance to be updated.
            validated_data (dict): The validated data for the update.

        Returns:
            Account: The updated account instance.

        If terms_accepted is provided and true, this method ensures that the AccountDetails
        instance is updated accordingly, marking terms as accepted.
        If AccountDetails doesn't exist, it will be created.
        """

        account_details_data = validated_data.get("account_details", {})

        if account_details_data.get("terms_accepted"):
            account_details, created = AccountDetails.objects.get_or_create(account=instance)
            account_details.terms_accepted = True
            account_details.save(update_fields=["terms_accepted"])

        return instance

    class Meta:
        """
        Meta class to define the model and fields to be used in the serializer.

        model (Model): Specifies the Account model that this serializer relates to.
        fields (tuple): Defines the fields to be included in the serialized output.
        read_only_fields (tuple): Fields that should not be edited through the serializer.
        """

        model = Account
        fields = (
            "crsid",
            "date_of_birth",
            "name",
            "affiliation",
            "college",
            "terms_accepted",
        )
        read_only_fields = (
            "crsid",
            "date_of_birth",
            "name",
            "affiliation",
            "college",
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        account_identifier = AccountIdentifier.objects.filter(account_id=instance.pk).first()
        representation["date_of_birth"] = account_identifier.date_of_birth.strftime("%Y-%m-%d")
        return representation


class MethodNotAllowedErrorSerializer(serializers.Serializer):
    """
    Represents an error response when attempting an unsupported operation.
    """

    detail = serializers.CharField()


class ResetTokenSerializer(serializers.Serializer):
    """
    Provides a Password App reset token
    """

    token = serializers.CharField()
