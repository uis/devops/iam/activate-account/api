"""
Views implementing the API endpoints.

"""

import random
from string import ascii_uppercase, digits

from django.conf import settings
from drf_spectacular.utils import OpenApiResponse, extend_schema
from rest_framework import exceptions, generics, status

from activate_account.reset_tokens import (
    PasswordAppNotFound,
    PasswordAppTokenResponse,
    get_reset_token,
)
from api.v1alpha.serializers import (
    AccountSerializer,
    MethodNotAllowedErrorSerializer,
    ResetTokenSerializer,
)


class AccountDetailsView(generics.RetrieveUpdateAPIView):
    serializer_class = AccountSerializer
    http_method_names = ["get", "patch", "options", "head"]

    def get_object(self):
        return self.request.user

    @extend_schema(
        tags=["account management"],
        summary="Retrieve Account Details",
        description="Retrieves the account details of the currently logged-in user.",
        responses={
            200: OpenApiResponse(
                response=AccountSerializer,
                description="Successful retrieval of account details.",
            ),
        },
        methods=["GET"],
        operation_id="getAccount",
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    @extend_schema(
        tags=["account management"],
        summary="Update Account Details",
        description="Updates the account details of the currently logged-in user.",
        responses={
            200: OpenApiResponse(
                response=AccountSerializer,
                description="Successful update of account details.",
            ),
            204: OpenApiResponse(
                description="Successful update with no content to return "
                "when updating certain fields."
            ),
            405: OpenApiResponse(
                response=MethodNotAllowedErrorSerializer,
                description="Attempted to perform an unsupported operation.",
            ),
        },
        methods=["PATCH"],
        operation_id="patchAccount",
    )
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)


class ResetTokenUnavailable(exceptions.APIException):
    status_code = status.HTTP_503_SERVICE_UNAVAILABLE
    default_detail = "Failed to retrieve reset token"
    default_code = "service_unavailable"


class ResetTokenView(generics.RetrieveAPIView):
    serializer_class = ResetTokenSerializer

    def get_object(self):
        try:
            return get_reset_token(self.request.user.crsid)
        except PasswordAppNotFound:
            # To aid demo/testing purposes, we can fake a reset token if the Password App can't
            # find the user
            if settings.FAKE_RESET_TOKEN_IF_MISSING:
                fake_token = "-".join(
                    [
                        "".join([random.choice(digits + ascii_uppercase) for _ in range(4)])
                        for _ in range(3)
                    ]
                    + ["FAKE"]  # Help developers identify fake tokens
                )
                return PasswordAppTokenResponse(token=fake_token)
            # Raising a validation error here rather than a 404 which could be misunderstand as
            # the endpoint not existing
            raise exceptions.ValidationError({"crsid": "Password App was unable to find the user"})
        except Exception:
            # Otherwise, report a 503 as this service is temporarily unavailable
            raise ResetTokenUnavailable

    @extend_schema(
        tags=["account management"],
        summary="Generate Reset Token",
        description="Generates a new reset token for the currently logged-in user.",
        responses={
            200: OpenApiResponse(
                response=ResetTokenSerializer,
                description="Successful generation of a new reset token.",
            ),
        },
        methods=["GET"],
        operation_id="resetToken",
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)
