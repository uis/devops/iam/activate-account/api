import pytest
from django.urls import reverse
from rest_framework import status


@pytest.fixture
def url():
    return reverse("v1alpha1:account")


def test_account_details(authenticated_api_client, account, account_details, url):
    response = authenticated_api_client.get(url)

    assert response.status_code == status.HTTP_200_OK

    date_of_birth = account.account_identifier.first().date_of_birth

    assert response.data == {
        # The `account` is returned from the factory, so the CRSId is still cased. The saved
        # version in the database is always lowercased.
        "crsid": account.crsid.lower(),
        "date_of_birth": date_of_birth.strftime("%Y-%m-%d"),
        "name": account_details.name,
        "affiliation": account_details.affiliation,
        "college": account_details.college,
        "terms_accepted": account_details.terms_accepted,
    }


def test_update_terms_accepted(authenticated_api_client, account, url):
    # Ensure AccountDetails does not exist initially
    assert not hasattr(account, "account_details"), "AccountDetails should not exist initially"

    # Send PATCH request to update the terms_accepted field to True
    update_data_true = {"terms_accepted": True}
    patch_response_true = authenticated_api_client.patch(url, update_data_true, format="json")

    # Check response and update status to True
    assert (
        patch_response_true.status_code == status.HTTP_200_OK
    ), f"Expected 200 OK, got {patch_response_true.status_code}"
    assert patch_response_true.data["terms_accepted"] is True, "terms_accepted should now be True"

    # Refresh the account instance and check if AccountDetails was created
    account.refresh_from_db()
    assert hasattr(account, "account_details"), "AccountDetails should have been created"

    assert (
        account.account_details.terms_accepted is True
    ), "terms_accepted should be True in the newly created AccountDetails instance"

    # Attempt to revert terms_accepted to False using PATCH
    update_data_false = {"terms_accepted": False}
    patch_response_false = authenticated_api_client.patch(url, update_data_false, format="json")

    # Check that the update to False is not accepted
    assert (
        patch_response_false.status_code == status.HTTP_200_OK
    ), f"Expected 200 OK, got {patch_response_false.status_code}"
    assert (
        patch_response_false.data["terms_accepted"] is True
    ), "terms_accepted should remain True, revert attempt should not succeed"

    # Use PUT method to attempt a full update including terms_accepted set to False
    full_data_put_false = {
        "terms_accepted": False,
        "name": "Updated Name",
        "affiliation": "Updated Affiliation",
        "college": "Updated College",
    }
    put_response_false = authenticated_api_client.put(url, full_data_put_false, format="json")

    # Check that the PUT method does not change terms_accepted to False
    assert (
        put_response_false.status_code == status.HTTP_405_METHOD_NOT_ALLOWED
    ), f"Expected 405, got {put_response_false.status_code}"

    # Fetch again via GET to confirm persistence of True state
    refreshed_response = authenticated_api_client.get(url)
    assert (
        refreshed_response.data["terms_accepted"] is True
    ), "Updated state should persist as True after refresh"
