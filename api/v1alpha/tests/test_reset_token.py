import re

import pytest
from django.conf import settings
from django.test import override_settings
from django.urls import reverse
from rest_framework import status

from activate_account.reset_tokens import PASSWORD_APP_RETRIES


@pytest.fixture
def url():
    return reverse("v1alpha1:reset-token")


@pytest.fixture
def reset_token_dict(reset_token):
    return reset_token.__dict__


@pytest.fixture
def successful_reset_token_response(requests_mock, reset_token_dict):
    return requests_mock.get(settings.PASSWORD_APP_RESET_TOKEN_URL, json=reset_token_dict)


def test_reset_token(
    authenticated_api_client, url, reset_token_dict, successful_reset_token_response
):
    response = authenticated_api_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert response.data == reset_token_dict


@pytest.fixture
def failed_reset_token_response(requests_mock):
    return requests_mock.get(
        settings.PASSWORD_APP_RESET_TOKEN_URL, status_code=500, reason="Password App Down"
    )


def test_failed_reset_token(authenticated_api_client, url, failed_reset_token_response):
    response = authenticated_api_client.get(url)

    assert response.status_code == status.HTTP_503_SERVICE_UNAVAILABLE
    # Initial call then PASSWORD_APP_RETRIES retries
    assert failed_reset_token_response.call_count == PASSWORD_APP_RETRIES + 1


@pytest.fixture
def missing_user_reset_token_response(requests_mock):
    return requests_mock.get(
        settings.PASSWORD_APP_RESET_TOKEN_URL, status_code=404, reason="User not found"
    )


def test_missing_user_reset_token(
    authenticated_api_client, url, missing_user_reset_token_response
):
    response = authenticated_api_client.get(url)

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    # Call once and not retried
    assert missing_user_reset_token_response.call_count == 1


@override_settings(FAKE_RESET_TOKEN_IF_MISSING=True)
def test_missing_user_fake_reset_token(
    authenticated_api_client, url, missing_user_reset_token_response
):
    response = authenticated_api_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    # Looks like a reset token but with FAKE at the end
    assert re.fullmatch(r"([0-9A-Z]{4}-){3}FAKE", response.data["token"])
