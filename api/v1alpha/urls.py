"""
URL routing schema for API

"""

from django.urls import include, path
from rest_framework import routers

from api.v1alpha.views import AccountDetailsView, ResetTokenView

app_name = "v1alpha"

router = routers.DefaultRouter()

urlpatterns = [
    path("", include(router.urls)),
    path("account/", AccountDetailsView.as_view(), name="account"),
    path("reset-token/", ResetTokenView.as_view(), name="reset-token"),
]
